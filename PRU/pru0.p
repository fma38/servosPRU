/* pru0.p (for cape v1.0)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  or see http://www.gnu.org/licenses/gpl.html

  author: Frédéric Mantegazza
  copyright: (C) 2015-2016 Frédéric Mantegazza
  license: GPL

PRU0 has 3 tasks:
 - generating fixes pulses widths
 - changing pulses widths of some servos in a sync way, so they start/stop at the same time
 - read inputs, and stop matching group of servos when activated

Communication with host is done through Data Memory. For 32 servos:
 - [0x00000000:0x0000007f]: current pulses widths
 - [0x00000080:0x000000ff]: increments
 - [0x00000100:0x00000103]: nb steps
 - [0x00000104:0x00000123]: inputs

Values are stored as words (4 bytes), in little endian order.

Pulses widths must be left shifted by 16bits. This is because we use 32bits for internal computation
when incrementing, but only use the MSB 16bits for effective value.

Increments are not shifted.

Changing current pulses widths values immediatly change servo position (no speed management).
Use value 0 to disable the servo.

To set new positions with speed management, set increments values for the given servos, and set nb steps.
Setting nb steps starts move.
While moving, nb steps is decremented by the PRU. Monitor it to know when the move ends (0), or wait for event.

Be carefull not to modify current pulses widths while moving!

Inputs act as servos endstops ; each input matches a group of 4 servos.
When an input is activated, the increments of the matching group is set to zero.
This is mainly used to act as multipod legs endstops.

The inputs values stored in memory can be read, but for now, are only refreshed during a move.
*/

.origin 0
.entrypoint START

#include "servos.hp"
#include "macros.hp"


START:

    // Enable OCP master port, to allow communication with ARM bus
    LBCO    r1, C4, 4, 4        // load content of PRU_ICSS_CONFIG register (pointed by C4) into r0
    CLR     r1, r1, 4           // clear SYSCFG[STANDBY_INIT], which enables OCP master port
    SBCO    r1, C4, 4, 4        // store back PRU_ICSS_CONFIG register content

    // Configure Programmable Pointer Register C28 to point to PRUs shared RAM
    LDI     r1, 0x100
    MOV     r2, PRU0_CTRL       // set C28_pointer[15:0] field to 0x00000100,
    SBBO    r1, r2, CTPPR_0, 4  // so C28 points to 0x00010000 (shared RAM)

    // Clear pulses widths in Data Memory at offset [0x00:0x7f] (pointed by C24)
    ZERO    4, NB_SERVOS/2*4  // load 0 into r1-r16
    SBCO    r1, C24, PULSE_WIDTH_OFFSET, NB_SERVOS/2*4
    SBCO    r1, C24, PULSE_WIDTH_OFFSET + NB_SERVOS/2*4, NB_SERVOS/2*4

    // Clear pulses increments in Data Memory at offset [0x80:0xff]
    SBCO    r1, C24, INCREMENT_OFFSET, NB_SERVOS/2*4
    SBCO    r1, C24, INCREMENT_OFFSET + NB_SERVOS/2*4, NB_SERVOS/2*4

    // Clear nb steps into Data Memory at offset [0x100:0x103]
    // Can't use C24, here, as offset > 255
    LDI     r2, DATA_MEMORY + NB_STEPS_OFFSET
    SBBO    r1, r2, 0, 4

    // Clear inputs values into Data Memory at offset [0x104:0x124]
    // Can't use C24, here, as offset > 255
    ZERO    4, NB_INPUTS*4  // load 0 into r1-r8
    LDI     r9, DATA_MEMORY + INPUT_OFFSET
    SBBO    r1, r9, 0, NB_INPUTS*4

// Main loop, which represent 1 servo refresh cycle (20ms @ 50Hz)
MAIN_LOOP:

    // Load pulses widths values in registers from PRU0 Data Memory (pointed by C24),
    // and save them to scratch pad banks (1 bank for each group of servos)
    LBCO    r0, C24, PULSE_WIDTH_OFFSET, NB_SERVOS/2*4                                       // (1 + NB_SERVOS/2) * 5ns
    XOUT    PULSES_0_BANK_ID, r0, NB_SERVOS/2*4                                                                 //  5ns
    LBCO    r0, C24, PULSE_WIDTH_OFFSET + NB_SERVOS/2*4, NB_SERVOS/2*4                       // (1 + NB_SERVOS/2) * 5ns
    XOUT    PULSES_1_BANK_ID, r0, NB_SERVOS/2*4                                                                 //  5ns

    //
    // Servos 0-15 pulses generation loop
    //

    // Load counter for max pulse length wait loop (5000µs)
    // TODO: will have to ensure that setpoints are lower or equal to that value
    LDI     r16, 5000                                                                                           //  5ns

    // Retreive counters for servos 0-15
    XIN     PULSES_0_BANK_ID, r0, NB_SERVOS/2*4                                                                 //  5ns

    // Turn on servos 0-15 outputs
    pulse_on    r0,  SERVO_00_GPIO, SERVO_00_BIT                                                                // 40ns
    pulse_on    r1,  SERVO_01_GPIO, SERVO_01_BIT
    pulse_on    r2,  SERVO_02_GPIO, SERVO_02_BIT
    pulse_on    r3,  SERVO_03_GPIO, SERVO_03_BIT
    pulse_on    r4,  SERVO_04_GPIO, SERVO_04_BIT
    pulse_on    r5,  SERVO_05_GPIO, SERVO_05_BIT
    pulse_on    r6,  SERVO_06_GPIO, SERVO_06_BIT
    pulse_on    r7,  SERVO_07_GPIO, SERVO_07_BIT
    pulse_on    r8,  SERVO_08_GPIO, SERVO_08_BIT
    pulse_on    r9,  SERVO_09_GPIO, SERVO_09_BIT
    pulse_on    r10, SERVO_10_GPIO, SERVO_10_BIT
    pulse_on    r11, SERVO_11_GPIO, SERVO_11_BIT
    pulse_on    r12, SERVO_12_GPIO, SERVO_12_BIT
    pulse_on    r13, SERVO_13_GPIO, SERVO_13_BIT
    pulse_on    r14, SERVO_14_GPIO, SERVO_14_BIT
    pulse_on    r15, SERVO_15_GPIO, SERVO_15_BIT

    // First µs
    wait    END_OF_STEP_DELAY

    PULSES_LOOP_0:
        pulse   r0,  SERVO_00_GPIO, SERVO_00_BIT                                                                // 40ns
        pulse   r1,  SERVO_01_GPIO, SERVO_01_BIT
        pulse   r2,  SERVO_02_GPIO, SERVO_02_BIT
        pulse   r3,  SERVO_03_GPIO, SERVO_03_BIT
        pulse   r4,  SERVO_04_GPIO, SERVO_04_BIT
        pulse   r5,  SERVO_05_GPIO, SERVO_05_BIT
        pulse   r6,  SERVO_06_GPIO, SERVO_06_BIT
        pulse   r7,  SERVO_07_GPIO, SERVO_07_BIT
        pulse   r8,  SERVO_08_GPIO, SERVO_08_BIT
        pulse   r9,  SERVO_09_GPIO, SERVO_09_BIT
        pulse   r10, SERVO_10_GPIO, SERVO_10_BIT
        pulse   r11, SERVO_11_GPIO, SERVO_11_BIT
        pulse   r12, SERVO_12_GPIO, SERVO_12_BIT
        pulse   r13, SERVO_13_GPIO, SERVO_13_BIT
        pulse   r14, SERVO_14_GPIO, SERVO_14_BIT
        pulse   r15, SERVO_15_GPIO, SERVO_15_BIT

        // Wait end of pulse resolution (1µs)
        wait    END_OF_STEP_DELAY

        // Max pulse length counter loop
        SUB     r16, r16, 1                                                                                     //  5ns
        QBNE    PULSES_LOOP_0, r16, 0                                                                           //  5ns

    //
    // Servos 16-31 pulses generation loop
    //

    // Load counter for max pulse length wait loop (5000µs)
    // TODO: will have to ensure that setpoints are lower or equal to that value
    LDI     r16, 5000                                                                                           //  5ns

    // Retreive counters for servos 16-31
    XIN     PULSES_1_BANK_ID, r0, NB_SERVOS/2*4                                                                 //  5ns

    // Turn on servos 16-31 outputs
    pulse_on    r0,  SERVO_16_GPIO, SERVO_16_BIT                                                                // 40ns
    pulse_on    r1,  SERVO_17_GPIO, SERVO_17_BIT
    pulse_on    r2,  SERVO_18_GPIO, SERVO_18_BIT
    pulse_on    r3,  SERVO_19_GPIO, SERVO_19_BIT
    pulse_on    r4,  SERVO_20_GPIO, SERVO_20_BIT
    pulse_on    r5,  SERVO_21_GPIO, SERVO_21_BIT
    pulse_on    r6,  SERVO_22_GPIO, SERVO_22_BIT
    pulse_on    r7,  SERVO_23_GPIO, SERVO_23_BIT
    pulse_on    r8,  SERVO_24_GPIO, SERVO_24_BIT
    pulse_on    r9,  SERVO_25_GPIO, SERVO_25_BIT
    pulse_on    r10, SERVO_26_GPIO, SERVO_26_BIT
    pulse_on    r11, SERVO_27_GPIO, SERVO_27_BIT
    pulse_on    r12, SERVO_28_GPIO, SERVO_28_BIT
    pulse_on    r13, SERVO_29_GPIO, SERVO_29_BIT
    pulse_on    r14, SERVO_30_GPIO, SERVO_30_BIT
    pulse_on    r15, SERVO_31_GPIO, SERVO_31_BIT

    // First µs
    wait    END_OF_STEP_DELAY

    PULSES_LOOP_1:
        pulse   r0,  SERVO_16_GPIO, SERVO_16_BIT                                                                // 40ns
        pulse   r1,  SERVO_17_GPIO, SERVO_17_BIT
        pulse   r2,  SERVO_18_GPIO, SERVO_18_BIT
        pulse   r3,  SERVO_19_GPIO, SERVO_19_BIT
        pulse   r4,  SERVO_20_GPIO, SERVO_20_BIT
        pulse   r5,  SERVO_21_GPIO, SERVO_21_BIT
        pulse   r6,  SERVO_22_GPIO, SERVO_22_BIT
        pulse   r7,  SERVO_23_GPIO, SERVO_23_BIT
        pulse   r8,  SERVO_24_GPIO, SERVO_24_BIT
        pulse   r9,  SERVO_25_GPIO, SERVO_25_BIT
        pulse   r10, SERVO_26_GPIO, SERVO_26_BIT
        pulse   r11, SERVO_27_GPIO, SERVO_27_BIT
        pulse   r12, SERVO_28_GPIO, SERVO_28_BIT
        pulse   r13, SERVO_29_GPIO, SERVO_29_BIT
        pulse   r14, SERVO_30_GPIO, SERVO_30_BIT
        pulse   r15, SERVO_31_GPIO, SERVO_31_BIT

        // Wait end of pulse resolution (1µs)
        wait    END_OF_STEP_DELAY

        // Max pulse length counter loop
        SUB     r16, r16, 1                                                                                     //  5ns
        QBNE    PULSES_LOOP_1, r16, 0                                                                           //  5ns

    //
    // Move
    //

    // Check nb steps
    LDI     r2, DATA_MEMORY + NB_STEPS_OFFSET                                                                   //  5ns
    LBBO    r1, r2, 0, 4                                                                                   // 5ns + 5ns
    QBNE    MOVE, r1, 0                                                                                         //  5ns

    // Wait end of cycle (20ms)
    wait    END_OF_CYCLE_DELAY_0

    JMP     MAIN_LOOP                                                                                           //  5ns

MOVE:

    // Check inputs
    check   0                                                                                                   // 85ns
    check   1
    check   2
    check   3
    check   4
    check   5
    check   6
    check   7

    // Increment each pulse width
    step     0                                                                                                  // 35ns
    step     1
    step     2
    step     3
    step     4
    step     5
    step     6
    step     7
    step     8
    step     9
    step    10
    step    11
    step    12
    step    13
    step    14
    step    15
    step    16
    step    17
    step    18
    step    19
    step    20
    step    21
    step    22
    step    23
    step    24
    step    25
    step    26
    step    27
    step    28
    step    29
    step    30
    step    31

    // Decrement nb steps and store back to memory
    SUB     r1, r1, 1                                                                                           //  5ns
    SBBO    r1, r2, 0, 4                                                                                   // 5ns + 5ns

    // Check if move completed
    QBNE    MOVE_CONT, r1, 0                                                                                     // 5ns

    // Clear pulses increments in Data Memory
    ZERO    4, NB_SERVOS/2*4  // load 0 into r1-r16                                                             //  5ns
    SBCO    r1, C24, INCREMENT_OFFSET, NB_SERVOS/2*4                                         // (1 + NB_SERVOS/2) * 5ns
    SBCO    r1, C24, INCREMENT_OFFSET + NB_SERVOS/2*4, NB_SERVOS/2*4                         // (1 + NB_SERVOS/2) * 5ns

    // Send interrupt to host
    MOV     R31.b0, PRU0_ARM_INTERRUPT+16

    // Wait end of cycle (20ms)
    wait    END_OF_CYCLE_DELAY_2                                                                                //  5ns

    JMP     MAIN_LOOP                                                                                           //  5ns

MOVE_CONT:

    // Wait end of cycle (20ms)
    wait    END_OF_CYCLE_DELAY_1

    JMP     MAIN_LOOP                                                                                           //  5ns
