PASM=pasm
PASM_FLAGS=-b -V3 -L -d -l
DTC=dtc
FILE_PRU_0=pru0
FILE_PRU_1=pru1
FILE_DTO=servos
DDR_SIZE=0x40000

.PHONY: root all \
        overlay pru \
        modprobe modunprobe \
        install_pru install_pru0 install_pru1 \
        uinstall_pru uinstall_pru0 uinstall_pru1 \
        load_pru load_pru0 load_pru1 \
        install_overlay uninstall_overlay \
        load_overlay unload_overlay \
        test clean

root:
	@test `id -u` -eq 0 || (echo "This script must be run as root" && exit 2)

all: overlay pru

overlay:
	$(DTC) -O dtb -o overlay/$(FILE_DTO)-00A0.dtbo -b 0 -@ overlay/$(FILE_DTO).dtso

pru: pru0 pru1

pru0:
	$(PASM) $(PASM_FLAGS) PRU/$(FILE_PRU_0).p PRU/$(FILE_PRU_0)

pru1:
	$(PASM) $(PASM_FLAGS) PRU/$(FILE_PRU_1).p PRU/$(FILE_PRU_1)

modprobe: root
	modprobe uio_pruss extram_pool_sz=$(DDR_SIZE)

modunprobe: root
	modprobe -r uio_pruss

install_pru: install_pru0 install_pru1

install_pru0: root
	cp PRU/$(FILE_PRU_0).bin /lib/firmware/servos-$(FILE_PRU_0).bin

install_pru1: root
	cp PRU/$(FILE_PRU_1).bin /lib/firmware/servos-$(FILE_PRU_1).bin

uninstall_pru: uninstall_pru0 uninstall_pru1

uninstall_pru0: root
	rm -f /lib/firmware/servos-$(FILE_PRU_0).bin

uninstall_pru1: root
	rm -f /lib/firmware/servos-$(FILE_PRU_1).bin

load_pru: load_pru0 load_pru1

load_pru0: root modprobe
	python/pru-loader.py 0 PRU/$(FILE_PRU_0).bin

load_pru1: root modprobe
	python/pru-loader.py 1 PRU/$(FILE_PRU_1).bin

install_overlay: root
	cp overlay/$(FILE_DTO)-00A0.dtbo /lib/firmware/

uninstall_overlay: root
	rm -f /lib/firmware/$(FILE_DTO)-00A0.dtbo

load_overlay: root
	echo $(FILE_DTO) >$(SLOTS)
	cat $(SLOTS)

unload_overlay: root
	echo -`cat $(SLOTS) | grep $(FILE_DTO) | cut -d: -f1 -` >$(SLOTS)
	cat $(SLOTS)

test:
	python python/servos.py test

clean:
	rm -f PRU/*.bin
	rm -f PRU/*.dbg
	rm -f PRU/*.lst
	rm -f PRU/*.txt
	rm -f overlay/*.dtbo
	rm -f python/*.pyc
