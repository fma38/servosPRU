<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="BeagleBone-cape">
<packages>
<package name="BBB-CAPE-FOOTPRINT">
<wire x1="0" y1="12.7" x2="0" y2="71.755" width="0.127" layer="20"/>
<wire x1="41.91" y1="0" x2="54.61" y2="12.7" width="0.127" layer="20" curve="90"/>
<wire x1="0" y1="12.7" x2="12.7" y2="0" width="0.127" layer="20" curve="90"/>
<wire x1="41.91" y1="0" x2="12.7" y2="0" width="0.127" layer="20"/>
<wire x1="54.61" y1="12.7" x2="54.61" y2="71.755" width="0.127" layer="20"/>
<wire x1="54.61" y1="71.755" x2="41.91" y2="71.755" width="0.127" layer="20"/>
<wire x1="12.7" y1="71.755" x2="0" y2="71.755" width="0.127" layer="20"/>
<wire x1="36.195" y1="66.04" x2="18.415" y2="66.04" width="0.127" layer="20"/>
<wire x1="36.195" y1="66.04" x2="41.91" y2="71.755" width="0.127" layer="20" curve="90"/>
<wire x1="18.415" y1="66.04" x2="12.7" y2="71.755" width="0.127" layer="20" curve="-90"/>
<wire x1="1.27" y1="66.04" x2="0.635" y2="65.405" width="0.1524" layer="21"/>
<wire x1="0.635" y1="65.405" x2="0.635" y2="64.135" width="0.1524" layer="21"/>
<wire x1="0.635" y1="64.135" x2="1.27" y2="63.5" width="0.1524" layer="21"/>
<wire x1="1.27" y1="63.5" x2="0.635" y2="62.865" width="0.1524" layer="21"/>
<wire x1="0.635" y1="62.865" x2="0.635" y2="61.595" width="0.1524" layer="21"/>
<wire x1="0.635" y1="61.595" x2="1.27" y2="60.96" width="0.1524" layer="21"/>
<wire x1="1.27" y1="60.96" x2="0.635" y2="60.325" width="0.1524" layer="21"/>
<wire x1="0.635" y1="60.325" x2="0.635" y2="59.055" width="0.1524" layer="21"/>
<wire x1="0.635" y1="57.785" x2="0.635" y2="56.515" width="0.1524" layer="21"/>
<wire x1="0.635" y1="56.515" x2="1.27" y2="55.88" width="0.1524" layer="21"/>
<wire x1="1.27" y1="55.88" x2="0.635" y2="55.245" width="0.1524" layer="21"/>
<wire x1="0.635" y1="55.245" x2="0.635" y2="53.975" width="0.1524" layer="21"/>
<wire x1="0.635" y1="53.975" x2="1.27" y2="53.34" width="0.1524" layer="21"/>
<wire x1="0.635" y1="68.58" x2="5.08" y2="68.58" width="0.1524" layer="21"/>
<wire x1="5.715" y1="67.945" x2="5.715" y2="66.675" width="0.1524" layer="21"/>
<wire x1="5.715" y1="66.675" x2="5.08" y2="66.04" width="0.1524" layer="21"/>
<wire x1="5.08" y1="66.04" x2="5.715" y2="65.405" width="0.1524" layer="21"/>
<wire x1="5.715" y1="65.405" x2="5.715" y2="64.135" width="0.1524" layer="21"/>
<wire x1="5.715" y1="64.135" x2="5.08" y2="63.5" width="0.1524" layer="21"/>
<wire x1="5.08" y1="63.5" x2="5.715" y2="62.865" width="0.1524" layer="21"/>
<wire x1="5.715" y1="62.865" x2="5.715" y2="61.595" width="0.1524" layer="21"/>
<wire x1="5.715" y1="61.595" x2="5.08" y2="60.96" width="0.1524" layer="21"/>
<wire x1="5.08" y1="60.96" x2="5.715" y2="60.325" width="0.1524" layer="21"/>
<wire x1="5.715" y1="60.325" x2="5.715" y2="59.055" width="0.1524" layer="21"/>
<wire x1="5.715" y1="57.785" x2="5.715" y2="56.515" width="0.1524" layer="21"/>
<wire x1="5.715" y1="56.515" x2="5.08" y2="55.88" width="0.1524" layer="21"/>
<wire x1="5.08" y1="55.88" x2="5.715" y2="55.245" width="0.1524" layer="21"/>
<wire x1="5.715" y1="55.245" x2="5.715" y2="53.975" width="0.1524" layer="21"/>
<wire x1="5.715" y1="53.975" x2="5.08" y2="53.34" width="0.1524" layer="21"/>
<wire x1="5.08" y1="53.34" x2="5.715" y2="52.705" width="0.1524" layer="21"/>
<wire x1="5.715" y1="52.705" x2="5.715" y2="51.435" width="0.1524" layer="21"/>
<wire x1="5.715" y1="51.435" x2="5.08" y2="50.8" width="0.1524" layer="21"/>
<wire x1="5.08" y1="50.8" x2="5.715" y2="50.165" width="0.1524" layer="21"/>
<wire x1="5.715" y1="50.165" x2="5.715" y2="48.895" width="0.1524" layer="21"/>
<wire x1="5.715" y1="48.895" x2="5.08" y2="48.26" width="0.1524" layer="21"/>
<wire x1="5.08" y1="48.26" x2="5.715" y2="47.625" width="0.1524" layer="21"/>
<wire x1="5.715" y1="47.625" x2="5.715" y2="46.355" width="0.1524" layer="21"/>
<wire x1="5.715" y1="45.085" x2="5.715" y2="43.815" width="0.1524" layer="21"/>
<wire x1="5.715" y1="43.815" x2="5.08" y2="43.18" width="0.1524" layer="21"/>
<wire x1="5.08" y1="43.18" x2="5.715" y2="42.545" width="0.1524" layer="21"/>
<wire x1="5.715" y1="42.545" x2="5.715" y2="41.275" width="0.1524" layer="21"/>
<wire x1="5.715" y1="41.275" x2="5.08" y2="40.64" width="0.1524" layer="21"/>
<wire x1="5.08" y1="40.64" x2="5.715" y2="40.005" width="0.1524" layer="21"/>
<wire x1="5.715" y1="40.005" x2="5.715" y2="38.735" width="0.1524" layer="21"/>
<wire x1="5.08" y1="38.1" x2="5.715" y2="38.735" width="0.1524" layer="21"/>
<wire x1="5.08" y1="38.1" x2="5.715" y2="37.465" width="0.1524" layer="21"/>
<wire x1="5.715" y1="36.195" x2="5.715" y2="37.465" width="0.1524" layer="21"/>
<wire x1="5.715" y1="36.195" x2="5.08" y2="35.56" width="0.1524" layer="21"/>
<wire x1="5.08" y1="35.56" x2="5.715" y2="34.925" width="0.1524" layer="21"/>
<wire x1="5.715" y1="33.655" x2="5.715" y2="34.925" width="0.1524" layer="21"/>
<wire x1="5.715" y1="31.115" x2="5.715" y2="32.385" width="0.1524" layer="21"/>
<wire x1="5.715" y1="31.115" x2="5.08" y2="30.48" width="0.1524" layer="21"/>
<wire x1="5.08" y1="30.48" x2="5.715" y2="29.845" width="0.1524" layer="21"/>
<wire x1="5.715" y1="28.575" x2="5.715" y2="29.845" width="0.1524" layer="21"/>
<wire x1="5.715" y1="28.575" x2="5.08" y2="27.94" width="0.1524" layer="21"/>
<wire x1="1.27" y1="27.94" x2="0.635" y2="28.575" width="0.1524" layer="21"/>
<wire x1="0.635" y1="29.845" x2="0.635" y2="28.575" width="0.1524" layer="21"/>
<wire x1="0.635" y1="29.845" x2="1.27" y2="30.48" width="0.1524" layer="21"/>
<wire x1="1.27" y1="30.48" x2="0.635" y2="31.115" width="0.1524" layer="21"/>
<wire x1="0.635" y1="32.385" x2="0.635" y2="31.115" width="0.1524" layer="21"/>
<wire x1="0.635" y1="33.655" x2="0.635" y2="34.925" width="0.1524" layer="21"/>
<wire x1="1.27" y1="35.56" x2="0.635" y2="34.925" width="0.1524" layer="21"/>
<wire x1="1.27" y1="35.56" x2="0.635" y2="36.195" width="0.1524" layer="21"/>
<wire x1="0.635" y1="36.195" x2="0.635" y2="37.465" width="0.1524" layer="21"/>
<wire x1="1.27" y1="38.1" x2="0.635" y2="37.465" width="0.1524" layer="21"/>
<wire x1="1.27" y1="38.1" x2="0.635" y2="38.735" width="0.1524" layer="21"/>
<wire x1="0.635" y1="38.735" x2="0.635" y2="40.005" width="0.1524" layer="21"/>
<wire x1="1.27" y1="40.64" x2="0.635" y2="40.005" width="0.1524" layer="21"/>
<wire x1="1.27" y1="40.64" x2="0.635" y2="41.275" width="0.1524" layer="21"/>
<wire x1="0.635" y1="41.275" x2="0.635" y2="42.545" width="0.1524" layer="21"/>
<wire x1="1.27" y1="43.18" x2="0.635" y2="42.545" width="0.1524" layer="21"/>
<wire x1="1.27" y1="43.18" x2="0.635" y2="43.815" width="0.1524" layer="21"/>
<wire x1="0.635" y1="43.815" x2="0.635" y2="45.085" width="0.1524" layer="21"/>
<wire x1="0.635" y1="46.355" x2="0.635" y2="47.625" width="0.1524" layer="21"/>
<wire x1="1.27" y1="48.26" x2="0.635" y2="47.625" width="0.1524" layer="21"/>
<wire x1="1.27" y1="48.26" x2="0.635" y2="48.895" width="0.1524" layer="21"/>
<wire x1="0.635" y1="48.895" x2="0.635" y2="50.165" width="0.1524" layer="21"/>
<wire x1="1.27" y1="50.8" x2="0.635" y2="50.165" width="0.1524" layer="21"/>
<wire x1="1.27" y1="50.8" x2="0.635" y2="51.435" width="0.1524" layer="21"/>
<wire x1="0.635" y1="51.435" x2="0.635" y2="52.705" width="0.1524" layer="21"/>
<wire x1="1.27" y1="53.34" x2="0.635" y2="52.705" width="0.1524" layer="21"/>
<wire x1="5.08" y1="66.04" x2="1.27" y2="66.04" width="0.1524" layer="21"/>
<wire x1="5.08" y1="63.5" x2="1.27" y2="63.5" width="0.1524" layer="21"/>
<wire x1="5.08" y1="60.96" x2="1.27" y2="60.96" width="0.1524" layer="21"/>
<wire x1="5.08" y1="58.42" x2="1.27" y2="58.42" width="0.1524" layer="21"/>
<wire x1="5.08" y1="55.88" x2="1.27" y2="55.88" width="0.1524" layer="21"/>
<wire x1="5.08" y1="53.34" x2="1.27" y2="53.34" width="0.1524" layer="21"/>
<wire x1="5.08" y1="50.8" x2="1.27" y2="50.8" width="0.1524" layer="21"/>
<wire x1="5.08" y1="48.26" x2="1.27" y2="48.26" width="0.1524" layer="21"/>
<wire x1="5.08" y1="45.72" x2="1.27" y2="45.72" width="0.1524" layer="21"/>
<wire x1="5.08" y1="43.18" x2="1.27" y2="43.18" width="0.1524" layer="21"/>
<wire x1="5.08" y1="40.64" x2="1.27" y2="40.64" width="0.1524" layer="21"/>
<wire x1="5.08" y1="38.1" x2="1.27" y2="38.1" width="0.1524" layer="21"/>
<wire x1="5.08" y1="35.56" x2="1.27" y2="35.56" width="0.1524" layer="21"/>
<wire x1="5.08" y1="33.02" x2="1.27" y2="33.02" width="0.1524" layer="21"/>
<wire x1="5.08" y1="30.48" x2="1.27" y2="30.48" width="0.1524" layer="21"/>
<wire x1="5.08" y1="27.94" x2="1.27" y2="27.94" width="0.1524" layer="21"/>
<wire x1="5.08" y1="27.94" x2="5.715" y2="27.305" width="0.1524" layer="21"/>
<wire x1="5.715" y1="26.035" x2="5.715" y2="27.305" width="0.1524" layer="21"/>
<wire x1="5.715" y1="26.035" x2="5.08" y2="25.4" width="0.1524" layer="21"/>
<wire x1="1.27" y1="25.4" x2="0.635" y2="26.035" width="0.1524" layer="21"/>
<wire x1="0.635" y1="27.305" x2="0.635" y2="26.035" width="0.1524" layer="21"/>
<wire x1="0.635" y1="27.305" x2="1.27" y2="27.94" width="0.1524" layer="21"/>
<wire x1="5.08" y1="25.4" x2="1.27" y2="25.4" width="0.1524" layer="21"/>
<wire x1="5.08" y1="25.4" x2="5.715" y2="24.765" width="0.1524" layer="21"/>
<wire x1="5.715" y1="23.495" x2="5.715" y2="24.765" width="0.1524" layer="21"/>
<wire x1="5.715" y1="23.495" x2="5.08" y2="22.86" width="0.1524" layer="21"/>
<wire x1="1.27" y1="22.86" x2="0.635" y2="23.495" width="0.1524" layer="21"/>
<wire x1="0.635" y1="24.765" x2="0.635" y2="23.495" width="0.1524" layer="21"/>
<wire x1="0.635" y1="24.765" x2="1.27" y2="25.4" width="0.1524" layer="21"/>
<wire x1="5.08" y1="22.86" x2="1.27" y2="22.86" width="0.1524" layer="21"/>
<wire x1="5.08" y1="22.86" x2="5.715" y2="22.225" width="0.1524" layer="21"/>
<wire x1="5.715" y1="20.955" x2="5.715" y2="22.225" width="0.1524" layer="21"/>
<wire x1="0.635" y1="22.225" x2="0.635" y2="20.955" width="0.1524" layer="21"/>
<wire x1="0.635" y1="22.225" x2="1.27" y2="22.86" width="0.1524" layer="21"/>
<wire x1="5.08" y1="20.32" x2="1.27" y2="20.32" width="0.1524" layer="21"/>
<wire x1="5.715" y1="18.415" x2="5.715" y2="19.685" width="0.1524" layer="21"/>
<wire x1="5.715" y1="18.415" x2="5.08" y2="17.78" width="0.1524" layer="21"/>
<wire x1="1.27" y1="17.78" x2="0.635" y2="18.415" width="0.1524" layer="21"/>
<wire x1="0.635" y1="19.685" x2="0.635" y2="18.415" width="0.1524" layer="21"/>
<wire x1="5.08" y1="17.78" x2="1.27" y2="17.78" width="0.1524" layer="21"/>
<wire x1="5.08" y1="17.78" x2="5.715" y2="17.145" width="0.1524" layer="21"/>
<wire x1="5.715" y1="15.875" x2="5.715" y2="17.145" width="0.1524" layer="21"/>
<wire x1="5.715" y1="15.875" x2="5.08" y2="15.24" width="0.1524" layer="21"/>
<wire x1="1.27" y1="15.24" x2="0.635" y2="15.875" width="0.1524" layer="21"/>
<wire x1="0.635" y1="17.145" x2="0.635" y2="15.875" width="0.1524" layer="21"/>
<wire x1="0.635" y1="17.145" x2="1.27" y2="17.78" width="0.1524" layer="21"/>
<wire x1="5.08" y1="15.24" x2="1.27" y2="15.24" width="0.1524" layer="21"/>
<wire x1="5.08" y1="15.24" x2="5.715" y2="14.605" width="0.1524" layer="21"/>
<wire x1="5.715" y1="13.335" x2="5.715" y2="14.605" width="0.1524" layer="21"/>
<wire x1="0.635" y1="14.605" x2="0.635" y2="13.335" width="0.1524" layer="21"/>
<wire x1="0.635" y1="14.605" x2="1.27" y2="15.24" width="0.1524" layer="21"/>
<wire x1="5.08" y1="12.7" x2="1.27" y2="12.7" width="0.1524" layer="21"/>
<wire x1="5.715" y1="10.795" x2="5.715" y2="12.065" width="0.1524" layer="21"/>
<wire x1="5.715" y1="10.795" x2="5.08" y2="10.16" width="0.1524" layer="21"/>
<wire x1="1.27" y1="10.16" x2="0.635" y2="10.795" width="0.1524" layer="21"/>
<wire x1="0.635" y1="12.065" x2="0.635" y2="10.795" width="0.1524" layer="21"/>
<wire x1="5.08" y1="10.16" x2="1.27" y2="10.16" width="0.1524" layer="21"/>
<wire x1="0.635" y1="66.04" x2="1.27" y2="66.04" width="0.1524" layer="21"/>
<wire x1="0.635" y1="66.04" x2="0.635" y2="68.58" width="0.1524" layer="21"/>
<wire x1="5.08" y1="68.58" x2="5.715" y2="67.945" width="0.1524" layer="21"/>
<wire x1="5.08" y1="58.42" x2="5.715" y2="57.785" width="0.1524" layer="21"/>
<wire x1="5.715" y1="59.055" x2="5.08" y2="58.42" width="0.1524" layer="21"/>
<wire x1="1.27" y1="58.42" x2="0.635" y2="57.785" width="0.1524" layer="21"/>
<wire x1="0.635" y1="59.055" x2="1.27" y2="58.42" width="0.1524" layer="21"/>
<wire x1="5.08" y1="45.72" x2="5.715" y2="45.085" width="0.1524" layer="21"/>
<wire x1="5.715" y1="46.355" x2="5.08" y2="45.72" width="0.1524" layer="21"/>
<wire x1="1.27" y1="45.72" x2="0.635" y2="45.085" width="0.1524" layer="21"/>
<wire x1="1.27" y1="45.72" x2="0.635" y2="46.355" width="0.1524" layer="21"/>
<wire x1="5.08" y1="33.02" x2="5.715" y2="32.385" width="0.1524" layer="21"/>
<wire x1="5.715" y1="33.655" x2="5.08" y2="33.02" width="0.1524" layer="21"/>
<wire x1="1.27" y1="33.02" x2="0.635" y2="32.385" width="0.1524" layer="21"/>
<wire x1="1.27" y1="33.02" x2="0.635" y2="33.655" width="0.1524" layer="21"/>
<wire x1="5.08" y1="20.32" x2="5.715" y2="19.685" width="0.1524" layer="21"/>
<wire x1="5.715" y1="20.955" x2="5.08" y2="20.32" width="0.1524" layer="21"/>
<wire x1="0.635" y1="19.685" x2="1.27" y2="20.32" width="0.1524" layer="21"/>
<wire x1="1.27" y1="20.32" x2="0.635" y2="20.955" width="0.1524" layer="21"/>
<wire x1="5.08" y1="12.7" x2="5.715" y2="12.065" width="0.1524" layer="21"/>
<wire x1="5.715" y1="13.335" x2="5.08" y2="12.7" width="0.1524" layer="21"/>
<wire x1="0.635" y1="12.065" x2="1.27" y2="12.7" width="0.1524" layer="21"/>
<wire x1="1.27" y1="12.7" x2="0.635" y2="13.335" width="0.1524" layer="21"/>
<wire x1="49.53" y1="66.04" x2="48.895" y2="65.405" width="0.1524" layer="21"/>
<wire x1="48.895" y1="65.405" x2="48.895" y2="64.135" width="0.1524" layer="21"/>
<wire x1="48.895" y1="64.135" x2="49.53" y2="63.5" width="0.1524" layer="21"/>
<wire x1="49.53" y1="63.5" x2="48.895" y2="62.865" width="0.1524" layer="21"/>
<wire x1="48.895" y1="62.865" x2="48.895" y2="61.595" width="0.1524" layer="21"/>
<wire x1="48.895" y1="61.595" x2="49.53" y2="60.96" width="0.1524" layer="21"/>
<wire x1="49.53" y1="60.96" x2="48.895" y2="60.325" width="0.1524" layer="21"/>
<wire x1="48.895" y1="60.325" x2="48.895" y2="59.055" width="0.1524" layer="21"/>
<wire x1="48.895" y1="57.785" x2="48.895" y2="56.515" width="0.1524" layer="21"/>
<wire x1="48.895" y1="56.515" x2="49.53" y2="55.88" width="0.1524" layer="21"/>
<wire x1="49.53" y1="55.88" x2="48.895" y2="55.245" width="0.1524" layer="21"/>
<wire x1="48.895" y1="55.245" x2="48.895" y2="53.975" width="0.1524" layer="21"/>
<wire x1="48.895" y1="53.975" x2="49.53" y2="53.34" width="0.1524" layer="21"/>
<wire x1="48.895" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="21"/>
<wire x1="53.975" y1="67.945" x2="53.975" y2="66.675" width="0.1524" layer="21"/>
<wire x1="53.975" y1="66.675" x2="53.34" y2="66.04" width="0.1524" layer="21"/>
<wire x1="53.34" y1="66.04" x2="53.975" y2="65.405" width="0.1524" layer="21"/>
<wire x1="53.975" y1="65.405" x2="53.975" y2="64.135" width="0.1524" layer="21"/>
<wire x1="53.975" y1="64.135" x2="53.34" y2="63.5" width="0.1524" layer="21"/>
<wire x1="53.34" y1="63.5" x2="53.975" y2="62.865" width="0.1524" layer="21"/>
<wire x1="53.975" y1="62.865" x2="53.975" y2="61.595" width="0.1524" layer="21"/>
<wire x1="53.975" y1="61.595" x2="53.34" y2="60.96" width="0.1524" layer="21"/>
<wire x1="53.34" y1="60.96" x2="53.975" y2="60.325" width="0.1524" layer="21"/>
<wire x1="53.975" y1="60.325" x2="53.975" y2="59.055" width="0.1524" layer="21"/>
<wire x1="53.975" y1="57.785" x2="53.975" y2="56.515" width="0.1524" layer="21"/>
<wire x1="53.975" y1="56.515" x2="53.34" y2="55.88" width="0.1524" layer="21"/>
<wire x1="53.34" y1="55.88" x2="53.975" y2="55.245" width="0.1524" layer="21"/>
<wire x1="53.975" y1="55.245" x2="53.975" y2="53.975" width="0.1524" layer="21"/>
<wire x1="53.975" y1="53.975" x2="53.34" y2="53.34" width="0.1524" layer="21"/>
<wire x1="53.34" y1="53.34" x2="53.975" y2="52.705" width="0.1524" layer="21"/>
<wire x1="53.975" y1="52.705" x2="53.975" y2="51.435" width="0.1524" layer="21"/>
<wire x1="53.975" y1="51.435" x2="53.34" y2="50.8" width="0.1524" layer="21"/>
<wire x1="53.34" y1="50.8" x2="53.975" y2="50.165" width="0.1524" layer="21"/>
<wire x1="53.975" y1="50.165" x2="53.975" y2="48.895" width="0.1524" layer="21"/>
<wire x1="53.975" y1="48.895" x2="53.34" y2="48.26" width="0.1524" layer="21"/>
<wire x1="53.34" y1="48.26" x2="53.975" y2="47.625" width="0.1524" layer="21"/>
<wire x1="53.975" y1="47.625" x2="53.975" y2="46.355" width="0.1524" layer="21"/>
<wire x1="53.975" y1="45.085" x2="53.975" y2="43.815" width="0.1524" layer="21"/>
<wire x1="53.975" y1="43.815" x2="53.34" y2="43.18" width="0.1524" layer="21"/>
<wire x1="53.34" y1="43.18" x2="53.975" y2="42.545" width="0.1524" layer="21"/>
<wire x1="53.975" y1="42.545" x2="53.975" y2="41.275" width="0.1524" layer="21"/>
<wire x1="53.975" y1="41.275" x2="53.34" y2="40.64" width="0.1524" layer="21"/>
<wire x1="53.34" y1="40.64" x2="53.975" y2="40.005" width="0.1524" layer="21"/>
<wire x1="53.975" y1="40.005" x2="53.975" y2="38.735" width="0.1524" layer="21"/>
<wire x1="53.34" y1="38.1" x2="53.975" y2="38.735" width="0.1524" layer="21"/>
<wire x1="53.34" y1="38.1" x2="53.975" y2="37.465" width="0.1524" layer="21"/>
<wire x1="53.975" y1="36.195" x2="53.975" y2="37.465" width="0.1524" layer="21"/>
<wire x1="53.975" y1="36.195" x2="53.34" y2="35.56" width="0.1524" layer="21"/>
<wire x1="53.34" y1="35.56" x2="53.975" y2="34.925" width="0.1524" layer="21"/>
<wire x1="53.975" y1="33.655" x2="53.975" y2="34.925" width="0.1524" layer="21"/>
<wire x1="53.975" y1="31.115" x2="53.975" y2="32.385" width="0.1524" layer="21"/>
<wire x1="53.975" y1="31.115" x2="53.34" y2="30.48" width="0.1524" layer="21"/>
<wire x1="53.34" y1="30.48" x2="53.975" y2="29.845" width="0.1524" layer="21"/>
<wire x1="53.975" y1="28.575" x2="53.975" y2="29.845" width="0.1524" layer="21"/>
<wire x1="53.975" y1="28.575" x2="53.34" y2="27.94" width="0.1524" layer="21"/>
<wire x1="49.53" y1="27.94" x2="48.895" y2="28.575" width="0.1524" layer="21"/>
<wire x1="48.895" y1="29.845" x2="48.895" y2="28.575" width="0.1524" layer="21"/>
<wire x1="48.895" y1="29.845" x2="49.53" y2="30.48" width="0.1524" layer="21"/>
<wire x1="49.53" y1="30.48" x2="48.895" y2="31.115" width="0.1524" layer="21"/>
<wire x1="48.895" y1="32.385" x2="48.895" y2="31.115" width="0.1524" layer="21"/>
<wire x1="48.895" y1="33.655" x2="48.895" y2="34.925" width="0.1524" layer="21"/>
<wire x1="49.53" y1="35.56" x2="48.895" y2="34.925" width="0.1524" layer="21"/>
<wire x1="49.53" y1="35.56" x2="48.895" y2="36.195" width="0.1524" layer="21"/>
<wire x1="48.895" y1="36.195" x2="48.895" y2="37.465" width="0.1524" layer="21"/>
<wire x1="49.53" y1="38.1" x2="48.895" y2="37.465" width="0.1524" layer="21"/>
<wire x1="49.53" y1="38.1" x2="48.895" y2="38.735" width="0.1524" layer="21"/>
<wire x1="48.895" y1="38.735" x2="48.895" y2="40.005" width="0.1524" layer="21"/>
<wire x1="49.53" y1="40.64" x2="48.895" y2="40.005" width="0.1524" layer="21"/>
<wire x1="49.53" y1="40.64" x2="48.895" y2="41.275" width="0.1524" layer="21"/>
<wire x1="48.895" y1="41.275" x2="48.895" y2="42.545" width="0.1524" layer="21"/>
<wire x1="49.53" y1="43.18" x2="48.895" y2="42.545" width="0.1524" layer="21"/>
<wire x1="49.53" y1="43.18" x2="48.895" y2="43.815" width="0.1524" layer="21"/>
<wire x1="48.895" y1="43.815" x2="48.895" y2="45.085" width="0.1524" layer="21"/>
<wire x1="48.895" y1="46.355" x2="48.895" y2="47.625" width="0.1524" layer="21"/>
<wire x1="49.53" y1="48.26" x2="48.895" y2="47.625" width="0.1524" layer="21"/>
<wire x1="49.53" y1="48.26" x2="48.895" y2="48.895" width="0.1524" layer="21"/>
<wire x1="48.895" y1="48.895" x2="48.895" y2="50.165" width="0.1524" layer="21"/>
<wire x1="49.53" y1="50.8" x2="48.895" y2="50.165" width="0.1524" layer="21"/>
<wire x1="49.53" y1="50.8" x2="48.895" y2="51.435" width="0.1524" layer="21"/>
<wire x1="48.895" y1="51.435" x2="48.895" y2="52.705" width="0.1524" layer="21"/>
<wire x1="49.53" y1="53.34" x2="48.895" y2="52.705" width="0.1524" layer="21"/>
<wire x1="53.34" y1="66.04" x2="49.53" y2="66.04" width="0.1524" layer="21"/>
<wire x1="53.34" y1="63.5" x2="49.53" y2="63.5" width="0.1524" layer="21"/>
<wire x1="53.34" y1="60.96" x2="49.53" y2="60.96" width="0.1524" layer="21"/>
<wire x1="53.34" y1="58.42" x2="49.53" y2="58.42" width="0.1524" layer="21"/>
<wire x1="53.34" y1="55.88" x2="49.53" y2="55.88" width="0.1524" layer="21"/>
<wire x1="53.34" y1="53.34" x2="49.53" y2="53.34" width="0.1524" layer="21"/>
<wire x1="53.34" y1="50.8" x2="49.53" y2="50.8" width="0.1524" layer="21"/>
<wire x1="53.34" y1="48.26" x2="49.53" y2="48.26" width="0.1524" layer="21"/>
<wire x1="53.34" y1="45.72" x2="49.53" y2="45.72" width="0.1524" layer="21"/>
<wire x1="53.34" y1="43.18" x2="49.53" y2="43.18" width="0.1524" layer="21"/>
<wire x1="53.34" y1="40.64" x2="49.53" y2="40.64" width="0.1524" layer="21"/>
<wire x1="53.34" y1="38.1" x2="49.53" y2="38.1" width="0.1524" layer="21"/>
<wire x1="53.34" y1="35.56" x2="49.53" y2="35.56" width="0.1524" layer="21"/>
<wire x1="53.34" y1="33.02" x2="49.53" y2="33.02" width="0.1524" layer="21"/>
<wire x1="53.34" y1="30.48" x2="49.53" y2="30.48" width="0.1524" layer="21"/>
<wire x1="53.34" y1="27.94" x2="49.53" y2="27.94" width="0.1524" layer="21"/>
<wire x1="53.34" y1="27.94" x2="53.975" y2="27.305" width="0.1524" layer="21"/>
<wire x1="53.975" y1="26.035" x2="53.975" y2="27.305" width="0.1524" layer="21"/>
<wire x1="53.975" y1="26.035" x2="53.34" y2="25.4" width="0.1524" layer="21"/>
<wire x1="49.53" y1="25.4" x2="48.895" y2="26.035" width="0.1524" layer="21"/>
<wire x1="48.895" y1="27.305" x2="48.895" y2="26.035" width="0.1524" layer="21"/>
<wire x1="48.895" y1="27.305" x2="49.53" y2="27.94" width="0.1524" layer="21"/>
<wire x1="53.34" y1="25.4" x2="49.53" y2="25.4" width="0.1524" layer="21"/>
<wire x1="53.34" y1="25.4" x2="53.975" y2="24.765" width="0.1524" layer="21"/>
<wire x1="53.975" y1="23.495" x2="53.975" y2="24.765" width="0.1524" layer="21"/>
<wire x1="53.975" y1="23.495" x2="53.34" y2="22.86" width="0.1524" layer="21"/>
<wire x1="49.53" y1="22.86" x2="48.895" y2="23.495" width="0.1524" layer="21"/>
<wire x1="48.895" y1="24.765" x2="48.895" y2="23.495" width="0.1524" layer="21"/>
<wire x1="48.895" y1="24.765" x2="49.53" y2="25.4" width="0.1524" layer="21"/>
<wire x1="53.34" y1="22.86" x2="49.53" y2="22.86" width="0.1524" layer="21"/>
<wire x1="53.34" y1="22.86" x2="53.975" y2="22.225" width="0.1524" layer="21"/>
<wire x1="53.975" y1="20.955" x2="53.975" y2="22.225" width="0.1524" layer="21"/>
<wire x1="48.895" y1="22.225" x2="48.895" y2="20.955" width="0.1524" layer="21"/>
<wire x1="48.895" y1="22.225" x2="49.53" y2="22.86" width="0.1524" layer="21"/>
<wire x1="53.34" y1="20.32" x2="49.53" y2="20.32" width="0.1524" layer="21"/>
<wire x1="53.975" y1="18.415" x2="53.975" y2="19.685" width="0.1524" layer="21"/>
<wire x1="53.975" y1="18.415" x2="53.34" y2="17.78" width="0.1524" layer="21"/>
<wire x1="49.53" y1="17.78" x2="48.895" y2="18.415" width="0.1524" layer="21"/>
<wire x1="48.895" y1="19.685" x2="48.895" y2="18.415" width="0.1524" layer="21"/>
<wire x1="53.34" y1="17.78" x2="49.53" y2="17.78" width="0.1524" layer="21"/>
<wire x1="53.34" y1="17.78" x2="53.975" y2="17.145" width="0.1524" layer="21"/>
<wire x1="53.975" y1="15.875" x2="53.975" y2="17.145" width="0.1524" layer="21"/>
<wire x1="53.975" y1="15.875" x2="53.34" y2="15.24" width="0.1524" layer="21"/>
<wire x1="49.53" y1="15.24" x2="48.895" y2="15.875" width="0.1524" layer="21"/>
<wire x1="48.895" y1="17.145" x2="48.895" y2="15.875" width="0.1524" layer="21"/>
<wire x1="48.895" y1="17.145" x2="49.53" y2="17.78" width="0.1524" layer="21"/>
<wire x1="53.34" y1="15.24" x2="49.53" y2="15.24" width="0.1524" layer="21"/>
<wire x1="53.34" y1="15.24" x2="53.975" y2="14.605" width="0.1524" layer="21"/>
<wire x1="53.975" y1="13.335" x2="53.975" y2="14.605" width="0.1524" layer="21"/>
<wire x1="48.895" y1="14.605" x2="48.895" y2="13.335" width="0.1524" layer="21"/>
<wire x1="48.895" y1="14.605" x2="49.53" y2="15.24" width="0.1524" layer="21"/>
<wire x1="53.34" y1="12.7" x2="49.53" y2="12.7" width="0.1524" layer="21"/>
<wire x1="53.975" y1="10.795" x2="53.975" y2="12.065" width="0.1524" layer="21"/>
<wire x1="53.975" y1="10.795" x2="53.34" y2="10.16" width="0.1524" layer="21"/>
<wire x1="49.53" y1="10.16" x2="48.895" y2="10.795" width="0.1524" layer="21"/>
<wire x1="48.895" y1="12.065" x2="48.895" y2="10.795" width="0.1524" layer="21"/>
<wire x1="53.34" y1="10.16" x2="49.53" y2="10.16" width="0.1524" layer="21"/>
<wire x1="48.895" y1="66.04" x2="49.53" y2="66.04" width="0.1524" layer="21"/>
<wire x1="48.895" y1="66.04" x2="48.895" y2="68.58" width="0.1524" layer="21"/>
<wire x1="53.34" y1="68.58" x2="53.975" y2="67.945" width="0.1524" layer="21"/>
<wire x1="53.34" y1="58.42" x2="53.975" y2="57.785" width="0.1524" layer="21"/>
<wire x1="53.975" y1="59.055" x2="53.34" y2="58.42" width="0.1524" layer="21"/>
<wire x1="49.53" y1="58.42" x2="48.895" y2="57.785" width="0.1524" layer="21"/>
<wire x1="48.895" y1="59.055" x2="49.53" y2="58.42" width="0.1524" layer="21"/>
<wire x1="53.34" y1="45.72" x2="53.975" y2="45.085" width="0.1524" layer="21"/>
<wire x1="53.975" y1="46.355" x2="53.34" y2="45.72" width="0.1524" layer="21"/>
<wire x1="49.53" y1="45.72" x2="48.895" y2="45.085" width="0.1524" layer="21"/>
<wire x1="49.53" y1="45.72" x2="48.895" y2="46.355" width="0.1524" layer="21"/>
<wire x1="53.34" y1="33.02" x2="53.975" y2="32.385" width="0.1524" layer="21"/>
<wire x1="53.975" y1="33.655" x2="53.34" y2="33.02" width="0.1524" layer="21"/>
<wire x1="49.53" y1="33.02" x2="48.895" y2="32.385" width="0.1524" layer="21"/>
<wire x1="49.53" y1="33.02" x2="48.895" y2="33.655" width="0.1524" layer="21"/>
<wire x1="53.34" y1="20.32" x2="53.975" y2="19.685" width="0.1524" layer="21"/>
<wire x1="53.975" y1="20.955" x2="53.34" y2="20.32" width="0.1524" layer="21"/>
<wire x1="48.895" y1="19.685" x2="49.53" y2="20.32" width="0.1524" layer="21"/>
<wire x1="49.53" y1="20.32" x2="48.895" y2="20.955" width="0.1524" layer="21"/>
<wire x1="53.34" y1="12.7" x2="53.975" y2="12.065" width="0.1524" layer="21"/>
<wire x1="53.975" y1="13.335" x2="53.34" y2="12.7" width="0.1524" layer="21"/>
<wire x1="48.895" y1="12.065" x2="49.53" y2="12.7" width="0.1524" layer="21"/>
<wire x1="49.53" y1="12.7" x2="48.895" y2="13.335" width="0.1524" layer="21"/>
<rectangle x1="49.911" y1="67.056" x2="50.419" y2="67.564" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="67.056" x2="52.959" y2="67.564" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="64.516" x2="52.959" y2="65.024" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="64.516" x2="50.419" y2="65.024" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="61.976" x2="52.959" y2="62.484" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="61.976" x2="50.419" y2="62.484" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="59.436" x2="52.959" y2="59.944" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="56.896" x2="52.959" y2="57.404" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="54.356" x2="52.959" y2="54.864" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="59.436" x2="50.419" y2="59.944" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="56.896" x2="50.419" y2="57.404" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="54.356" x2="50.419" y2="54.864" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="51.816" x2="52.959" y2="52.324" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="51.816" x2="50.419" y2="52.324" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="49.276" x2="52.959" y2="49.784" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="49.276" x2="50.419" y2="49.784" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="46.736" x2="52.959" y2="47.244" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="46.736" x2="50.419" y2="47.244" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="44.196" x2="52.959" y2="44.704" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="44.196" x2="50.419" y2="44.704" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="41.656" x2="52.959" y2="42.164" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="41.656" x2="50.419" y2="42.164" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="39.116" x2="52.959" y2="39.624" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="39.116" x2="50.419" y2="39.624" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="36.576" x2="52.959" y2="37.084" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="36.576" x2="50.419" y2="37.084" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="34.036" x2="52.959" y2="34.544" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="34.036" x2="50.419" y2="34.544" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="31.496" x2="52.959" y2="32.004" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="31.496" x2="50.419" y2="32.004" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="28.956" x2="52.959" y2="29.464" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="28.956" x2="50.419" y2="29.464" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="26.416" x2="52.959" y2="26.924" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="26.416" x2="50.419" y2="26.924" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="23.876" x2="52.959" y2="24.384" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="23.876" x2="50.419" y2="24.384" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="21.336" x2="52.959" y2="21.844" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="21.336" x2="50.419" y2="21.844" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="18.796" x2="52.959" y2="19.304" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="18.796" x2="50.419" y2="19.304" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="16.256" x2="52.959" y2="16.764" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="16.256" x2="50.419" y2="16.764" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="13.716" x2="52.959" y2="14.224" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="13.716" x2="50.419" y2="14.224" layer="51" rot="R270"/>
<rectangle x1="52.451" y1="11.176" x2="52.959" y2="11.684" layer="51" rot="R270"/>
<rectangle x1="49.911" y1="11.176" x2="50.419" y2="11.684" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="67.056" x2="2.159" y2="67.564" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="67.056" x2="4.699" y2="67.564" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="64.516" x2="4.699" y2="65.024" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="64.516" x2="2.159" y2="65.024" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="61.976" x2="4.699" y2="62.484" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="61.976" x2="2.159" y2="62.484" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="59.436" x2="4.699" y2="59.944" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="56.896" x2="4.699" y2="57.404" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="54.356" x2="4.699" y2="54.864" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="59.436" x2="2.159" y2="59.944" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="56.896" x2="2.159" y2="57.404" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="54.356" x2="2.159" y2="54.864" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="51.816" x2="4.699" y2="52.324" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="51.816" x2="2.159" y2="52.324" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="49.276" x2="4.699" y2="49.784" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="49.276" x2="2.159" y2="49.784" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="46.736" x2="4.699" y2="47.244" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="46.736" x2="2.159" y2="47.244" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="44.196" x2="4.699" y2="44.704" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="44.196" x2="2.159" y2="44.704" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="41.656" x2="4.699" y2="42.164" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="41.656" x2="2.159" y2="42.164" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="39.116" x2="4.699" y2="39.624" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="39.116" x2="2.159" y2="39.624" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="36.576" x2="4.699" y2="37.084" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="36.576" x2="2.159" y2="37.084" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="34.036" x2="4.699" y2="34.544" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="34.036" x2="2.159" y2="34.544" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="31.496" x2="4.699" y2="32.004" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="31.496" x2="2.159" y2="32.004" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="28.956" x2="4.699" y2="29.464" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="28.956" x2="2.159" y2="29.464" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="26.416" x2="4.699" y2="26.924" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="26.416" x2="2.159" y2="26.924" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="23.876" x2="4.699" y2="24.384" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="23.876" x2="2.159" y2="24.384" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="21.336" x2="4.699" y2="21.844" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="21.336" x2="2.159" y2="21.844" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="18.796" x2="4.699" y2="19.304" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="18.796" x2="2.159" y2="19.304" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="16.256" x2="4.699" y2="16.764" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="16.256" x2="2.159" y2="16.764" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="13.716" x2="4.699" y2="14.224" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="13.716" x2="2.159" y2="14.224" layer="51" rot="R270"/>
<rectangle x1="4.191" y1="11.176" x2="4.699" y2="11.684" layer="51" rot="R270"/>
<rectangle x1="1.651" y1="11.176" x2="2.159" y2="11.684" layer="51" rot="R270"/>
<rectangle x1="10.795" y1="0" x2="26.035" y2="17.145" layer="39" rot="R180"/>
<pad name="1" x="50.165" y="67.31" drill="1" shape="square" rot="R270"/>
<pad name="2" x="52.705" y="67.31" drill="1" rot="R270"/>
<pad name="3" x="50.165" y="64.77" drill="1" rot="R270"/>
<pad name="4" x="52.705" y="64.77" drill="1" rot="R270"/>
<pad name="5" x="50.165" y="62.23" drill="1" rot="R270"/>
<pad name="6" x="52.705" y="62.23" drill="1" rot="R270"/>
<pad name="7" x="50.165" y="59.69" drill="1" rot="R270"/>
<pad name="8" x="52.705" y="59.69" drill="1" rot="R270"/>
<pad name="9" x="50.165" y="57.15" drill="1" rot="R270"/>
<pad name="10" x="52.705" y="57.15" drill="1" rot="R270"/>
<pad name="11" x="50.165" y="54.61" drill="1" rot="R270"/>
<pad name="12" x="52.705" y="54.61" drill="1" rot="R270"/>
<pad name="13" x="50.165" y="52.07" drill="1" rot="R270"/>
<pad name="14" x="52.705" y="52.07" drill="1" rot="R270"/>
<pad name="15" x="50.165" y="49.53" drill="1" rot="R270"/>
<pad name="16" x="52.705" y="49.53" drill="1" rot="R270"/>
<pad name="17" x="50.165" y="46.99" drill="1" rot="R270"/>
<pad name="18" x="52.705" y="46.99" drill="1" rot="R270"/>
<pad name="19" x="50.165" y="44.45" drill="1" rot="R270"/>
<pad name="20" x="52.705" y="44.45" drill="1" rot="R270"/>
<pad name="21" x="50.165" y="41.91" drill="1" rot="R270"/>
<pad name="22" x="52.705" y="41.91" drill="1" rot="R270"/>
<pad name="23" x="50.165" y="39.37" drill="1" rot="R270"/>
<pad name="24" x="52.705" y="39.37" drill="1" rot="R270"/>
<pad name="25" x="50.165" y="36.83" drill="1" rot="R270"/>
<pad name="26" x="52.705" y="36.83" drill="1" rot="R270"/>
<pad name="27" x="50.165" y="34.29" drill="1" rot="R270"/>
<pad name="28" x="52.705" y="34.29" drill="1" rot="R270"/>
<pad name="29" x="50.165" y="31.75" drill="1" rot="R270"/>
<pad name="30" x="52.705" y="31.75" drill="1" rot="R270"/>
<pad name="31" x="50.165" y="29.21" drill="1" rot="R270"/>
<pad name="32" x="52.705" y="29.21" drill="1" rot="R270"/>
<pad name="33" x="50.165" y="26.67" drill="1" rot="R270"/>
<pad name="34" x="52.705" y="26.67" drill="1" rot="R270"/>
<pad name="35" x="50.165" y="24.13" drill="1" rot="R270"/>
<pad name="36" x="52.705" y="24.13" drill="1" rot="R270"/>
<pad name="37" x="50.165" y="21.59" drill="1" rot="R270"/>
<pad name="38" x="52.705" y="21.59" drill="1" rot="R270"/>
<pad name="39" x="50.165" y="19.05" drill="1" rot="R270"/>
<pad name="40" x="52.705" y="19.05" drill="1" rot="R270"/>
<pad name="41" x="50.165" y="16.51" drill="1" rot="R270"/>
<pad name="42" x="52.705" y="16.51" drill="1" rot="R270"/>
<pad name="43" x="50.165" y="13.97" drill="1" rot="R270"/>
<pad name="44" x="52.705" y="13.97" drill="1" rot="R270"/>
<pad name="45" x="50.165" y="11.43" drill="1" rot="R270"/>
<pad name="46" x="52.705" y="11.43" drill="1" rot="R270"/>
<pad name="47" x="1.905" y="67.31" drill="1" shape="square" rot="R270"/>
<pad name="48" x="4.445" y="67.31" drill="1" rot="R270"/>
<pad name="49" x="1.905" y="64.77" drill="1" rot="R270"/>
<pad name="50" x="4.445" y="64.77" drill="1" rot="R270"/>
<pad name="51" x="1.905" y="62.23" drill="1" rot="R270"/>
<pad name="52" x="4.445" y="62.23" drill="1" rot="R270"/>
<pad name="53" x="1.905" y="59.69" drill="1" rot="R270"/>
<pad name="54" x="4.445" y="59.69" drill="1" rot="R270"/>
<pad name="55" x="1.905" y="57.15" drill="1" rot="R270"/>
<pad name="56" x="4.445" y="57.15" drill="1" rot="R270"/>
<pad name="57" x="1.905" y="54.61" drill="1" rot="R270"/>
<pad name="58" x="4.445" y="54.61" drill="1" rot="R270"/>
<pad name="59" x="1.905" y="52.07" drill="1" rot="R270"/>
<pad name="60" x="4.445" y="52.07" drill="1" rot="R270"/>
<pad name="61" x="1.905" y="49.53" drill="1" rot="R270"/>
<pad name="62" x="4.445" y="49.53" drill="1" rot="R270"/>
<pad name="63" x="1.905" y="46.99" drill="1" rot="R270"/>
<pad name="64" x="4.445" y="46.99" drill="1" rot="R270"/>
<pad name="65" x="1.905" y="44.45" drill="1" rot="R270"/>
<pad name="66" x="4.445" y="44.45" drill="1" rot="R270"/>
<pad name="67" x="1.905" y="41.91" drill="1" rot="R270"/>
<pad name="68" x="4.445" y="41.91" drill="1" rot="R270"/>
<pad name="69" x="1.905" y="39.37" drill="1" rot="R270"/>
<pad name="70" x="4.445" y="39.37" drill="1" rot="R270"/>
<pad name="71" x="1.905" y="36.83" drill="1" rot="R270"/>
<pad name="72" x="4.445" y="36.83" drill="1" rot="R270"/>
<pad name="73" x="1.905" y="34.29" drill="1" rot="R270"/>
<pad name="74" x="4.445" y="34.29" drill="1" rot="R270"/>
<pad name="75" x="1.905" y="31.75" drill="1" rot="R270"/>
<pad name="76" x="4.445" y="31.75" drill="1" rot="R270"/>
<pad name="77" x="1.905" y="29.21" drill="1" rot="R270"/>
<pad name="78" x="4.445" y="29.21" drill="1" rot="R270"/>
<pad name="79" x="1.905" y="26.67" drill="1" rot="R270"/>
<pad name="80" x="4.445" y="26.67" drill="1" rot="R270"/>
<pad name="81" x="1.905" y="24.13" drill="1" rot="R270"/>
<pad name="82" x="4.445" y="24.13" drill="1" rot="R270"/>
<pad name="83" x="1.905" y="21.59" drill="1" rot="R270"/>
<pad name="84" x="4.445" y="21.59" drill="1" rot="R270"/>
<pad name="85" x="1.905" y="19.05" drill="1" rot="R270"/>
<pad name="86" x="4.445" y="19.05" drill="1" rot="R270"/>
<pad name="87" x="1.905" y="16.51" drill="1" rot="R270"/>
<pad name="88" x="4.445" y="16.51" drill="1" rot="R270"/>
<pad name="89" x="1.905" y="13.97" drill="1" rot="R270"/>
<pad name="90" x="4.445" y="13.97" drill="1" rot="R270"/>
<pad name="91" x="1.905" y="11.43" drill="1" rot="R270"/>
<pad name="92" x="4.445" y="11.43" drill="1" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="BBB-CAPE-SYMBOL">
<wire x1="-31.75" y1="2.54" x2="31.75" y2="2.54" width="0.4064" layer="94"/>
<wire x1="31.75" y1="2.54" x2="31.75" y2="63.5" width="0.4064" layer="94"/>
<wire x1="31.75" y1="63.5" x2="-31.75" y2="63.5" width="0.4064" layer="94"/>
<wire x1="-31.75" y1="63.5" x2="-31.75" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-31.75" y1="-63.5" x2="31.75" y2="-63.5" width="0.4064" layer="94"/>
<wire x1="31.75" y1="-63.5" x2="31.75" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="31.75" y1="-2.54" x2="-31.75" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-31.75" y1="-2.54" x2="-31.75" y2="-63.5" width="0.4064" layer="94"/>
<pin name="AIN0" x="-27.94" y="-53.34" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="AIN1" x="27.94" y="-53.34" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="AIN2" x="-27.94" y="-50.8" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="AIN3" x="27.94" y="-50.8" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="AIN4" x="-27.94" y="-45.72" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="AIN5" x="27.94" y="-48.26" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="AIN6" x="-27.94" y="-48.26" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="CLKOUT2" x="-27.94" y="-55.88" visible="pin" length="short" direction="in" function="dot"/>
<pin name="EHRPWM1A" x="27.94" y="-20.32" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="EHRPWM1B" x="27.94" y="-22.86" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="EHRPWM2A" x="-27.94" y="38.1" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="EHRPWM2B" x="-27.94" y="45.72" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GND@1" x="-27.94" y="-5.08" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="GND@2" x="27.94" y="-5.08" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="GND@3" x="-27.94" y="-58.42" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="GND@4" x="-27.94" y="-60.96" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="GND@5" x="27.94" y="-58.42" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="GND@6" x="27.94" y="-60.96" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="GND@7" x="-27.94" y="60.96" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="GND@8" x="27.94" y="60.96" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="GNDA_ADC" x="27.94" y="-45.72" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO0_7" x="27.94" y="-55.88" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO0_26" x="27.94" y="45.72" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO0_27" x="-27.94" y="40.64" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_0" x="-27.94" y="30.48" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_1" x="27.94" y="33.02" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_2" x="-27.94" y="55.88" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_3" x="27.94" y="55.88" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_4" x="-27.94" y="33.02" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_5" x="27.94" y="35.56" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_6" x="-27.94" y="58.42" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_7" x="27.94" y="58.42" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_12" x="27.94" y="48.26" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_13" x="-27.94" y="48.26" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_14" x="27.94" y="43.18" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_15" x="-27.94" y="43.18" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_16" x="-27.94" y="-22.86" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_17" x="-27.94" y="-33.02" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_28" x="27.94" y="-17.78" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_29" x="27.94" y="30.48" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO1_30" x="-27.94" y="35.56" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO1_31" x="27.94" y="38.1" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_1" x="27.94" y="40.64" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_6" x="-27.94" y="5.08" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_7" x="27.94" y="5.08" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_8" x="-27.94" y="7.62" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_9" x="27.94" y="7.62" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_10" x="-27.94" y="10.16" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_11" x="27.94" y="10.16" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_12" x="-27.94" y="12.7" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_13" x="27.94" y="12.7" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_22" x="-27.94" y="27.94" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_23" x="-27.94" y="25.4" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO2_24" x="27.94" y="27.94" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO2_25" x="27.94" y="25.4" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="GPIO3_19" x="-27.94" y="-38.1" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="GPIO3_21" x="-27.94" y="-35.56" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="I2C1_SCL" x="-27.94" y="-25.4" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="I2C1_SDA" x="27.94" y="-25.4" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="I2C2_SCL" x="-27.94" y="-27.94" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="I2C2_SDA" x="27.94" y="-27.94" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="PWR_BUT" x="-27.94" y="-15.24" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="SPI1_CS0" x="27.94" y="-38.1" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="SPI1_DI" x="27.94" y="-40.64" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="SPI1_DO" x="-27.94" y="-40.64" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="SPI1_SCLK" x="-27.94" y="-43.18" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="SYS_5V@1" x="-27.94" y="-12.7" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="SYS_5V@2" x="27.94" y="-12.7" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="SYS_RESETN" x="27.94" y="-15.24" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="TIMER4" x="-27.94" y="53.34" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="TIMER5" x="-27.94" y="50.8" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="TIMER6" x="27.94" y="50.8" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="TIMER7" x="27.94" y="53.34" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART1_RXD" x="27.94" y="-35.56" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART1_TXD" x="27.94" y="-33.02" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART2_RXD" x="27.94" y="-30.48" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART2_TXD" x="-27.94" y="-30.48" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART3_CTSN" x="27.94" y="17.78" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART3_RTSN" x="27.94" y="20.32" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART4_CTSN" x="-27.94" y="17.78" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART4_RTSN" x="-27.94" y="20.32" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART4_RXD" x="-27.94" y="-17.78" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART4_TXD" x="-27.94" y="-20.32" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART5_CTSN" x="-27.94" y="22.86" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="UART5_RTSN" x="27.94" y="22.86" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART5_RXD" x="27.94" y="15.24" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="UART5_TXD" x="-27.94" y="15.24" visible="pin" length="short" direction="pas" function="dot"/>
<pin name="VDD_3V3EXP@1" x="-27.94" y="-7.62" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="VDD_3V3EXP@2" x="27.94" y="-7.62" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="VDD_5V@1" x="-27.94" y="-10.16" visible="pin" length="short" direction="pwr" function="dot"/>
<pin name="VDD_5V@2" x="27.94" y="-10.16" visible="pin" length="short" direction="pwr" function="dot" rot="R180"/>
<pin name="VDD_ADC" x="27.94" y="-43.18" visible="pin" length="short" direction="pas" function="dot" rot="R180"/>
<text x="-30.48" y="66.04" size="2.54" layer="95" font="vector">P8</text>
<text x="-30.48" y="-68.58" size="2.54" layer="95" font="vector">P9</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BBB-CAPE" prefix="BBB">
<gates>
<gate name="G$1" symbol="BBB-CAPE-SYMBOL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BBB-CAPE-FOOTPRINT">
<connects>
<connect gate="G$1" pin="AIN0" pad="85"/>
<connect gate="G$1" pin="AIN1" pad="86"/>
<connect gate="G$1" pin="AIN2" pad="83"/>
<connect gate="G$1" pin="AIN3" pad="84"/>
<connect gate="G$1" pin="AIN4" pad="79"/>
<connect gate="G$1" pin="AIN5" pad="82"/>
<connect gate="G$1" pin="AIN6" pad="81"/>
<connect gate="G$1" pin="CLKOUT2" pad="87"/>
<connect gate="G$1" pin="EHRPWM1A" pad="60"/>
<connect gate="G$1" pin="EHRPWM1B" pad="62"/>
<connect gate="G$1" pin="EHRPWM2A" pad="19"/>
<connect gate="G$1" pin="EHRPWM2B" pad="13"/>
<connect gate="G$1" pin="GND@1" pad="47"/>
<connect gate="G$1" pin="GND@2" pad="48"/>
<connect gate="G$1" pin="GND@3" pad="89"/>
<connect gate="G$1" pin="GND@4" pad="91"/>
<connect gate="G$1" pin="GND@5" pad="90"/>
<connect gate="G$1" pin="GND@6" pad="92"/>
<connect gate="G$1" pin="GND@7" pad="1"/>
<connect gate="G$1" pin="GND@8" pad="2"/>
<connect gate="G$1" pin="GNDA_ADC" pad="80"/>
<connect gate="G$1" pin="GPIO0_26" pad="14"/>
<connect gate="G$1" pin="GPIO0_27" pad="17"/>
<connect gate="G$1" pin="GPIO0_7" pad="88"/>
<connect gate="G$1" pin="GPIO1_0" pad="25"/>
<connect gate="G$1" pin="GPIO1_1" pad="24"/>
<connect gate="G$1" pin="GPIO1_12" pad="12"/>
<connect gate="G$1" pin="GPIO1_13" pad="11"/>
<connect gate="G$1" pin="GPIO1_14" pad="16"/>
<connect gate="G$1" pin="GPIO1_15" pad="15"/>
<connect gate="G$1" pin="GPIO1_16" pad="61"/>
<connect gate="G$1" pin="GPIO1_17" pad="69"/>
<connect gate="G$1" pin="GPIO1_2" pad="5"/>
<connect gate="G$1" pin="GPIO1_28" pad="58"/>
<connect gate="G$1" pin="GPIO1_29" pad="26"/>
<connect gate="G$1" pin="GPIO1_3" pad="6"/>
<connect gate="G$1" pin="GPIO1_30" pad="21"/>
<connect gate="G$1" pin="GPIO1_31" pad="20"/>
<connect gate="G$1" pin="GPIO1_4" pad="23"/>
<connect gate="G$1" pin="GPIO1_5" pad="22"/>
<connect gate="G$1" pin="GPIO1_6" pad="3"/>
<connect gate="G$1" pin="GPIO1_7" pad="4"/>
<connect gate="G$1" pin="GPIO2_1" pad="18"/>
<connect gate="G$1" pin="GPIO2_10" pad="41"/>
<connect gate="G$1" pin="GPIO2_11" pad="42"/>
<connect gate="G$1" pin="GPIO2_12" pad="39"/>
<connect gate="G$1" pin="GPIO2_13" pad="40"/>
<connect gate="G$1" pin="GPIO2_22" pad="27"/>
<connect gate="G$1" pin="GPIO2_23" pad="29"/>
<connect gate="G$1" pin="GPIO2_24" pad="28"/>
<connect gate="G$1" pin="GPIO2_25" pad="30"/>
<connect gate="G$1" pin="GPIO2_6" pad="45"/>
<connect gate="G$1" pin="GPIO2_7" pad="46"/>
<connect gate="G$1" pin="GPIO2_8" pad="43"/>
<connect gate="G$1" pin="GPIO2_9" pad="44"/>
<connect gate="G$1" pin="GPIO3_19" pad="73"/>
<connect gate="G$1" pin="GPIO3_21" pad="71"/>
<connect gate="G$1" pin="I2C1_SCL" pad="63"/>
<connect gate="G$1" pin="I2C1_SDA" pad="64"/>
<connect gate="G$1" pin="I2C2_SCL" pad="65"/>
<connect gate="G$1" pin="I2C2_SDA" pad="66"/>
<connect gate="G$1" pin="PWR_BUT" pad="55"/>
<connect gate="G$1" pin="SPI1_CS0" pad="74"/>
<connect gate="G$1" pin="SPI1_DI" pad="76"/>
<connect gate="G$1" pin="SPI1_DO" pad="75"/>
<connect gate="G$1" pin="SPI1_SCLK" pad="77"/>
<connect gate="G$1" pin="SYS_5V@1" pad="53"/>
<connect gate="G$1" pin="SYS_5V@2" pad="54"/>
<connect gate="G$1" pin="SYS_RESETN" pad="56"/>
<connect gate="G$1" pin="TIMER4" pad="7"/>
<connect gate="G$1" pin="TIMER5" pad="9"/>
<connect gate="G$1" pin="TIMER6" pad="10"/>
<connect gate="G$1" pin="TIMER7" pad="8"/>
<connect gate="G$1" pin="UART1_RXD" pad="72"/>
<connect gate="G$1" pin="UART1_TXD" pad="70"/>
<connect gate="G$1" pin="UART2_RXD" pad="68"/>
<connect gate="G$1" pin="UART2_TXD" pad="67"/>
<connect gate="G$1" pin="UART3_CTSN" pad="36"/>
<connect gate="G$1" pin="UART3_RTSN" pad="34"/>
<connect gate="G$1" pin="UART4_CTSN" pad="35"/>
<connect gate="G$1" pin="UART4_RTSN" pad="33"/>
<connect gate="G$1" pin="UART4_RXD" pad="57"/>
<connect gate="G$1" pin="UART4_TXD" pad="59"/>
<connect gate="G$1" pin="UART5_CTSN" pad="31"/>
<connect gate="G$1" pin="UART5_RTSN" pad="32"/>
<connect gate="G$1" pin="UART5_RXD" pad="38"/>
<connect gate="G$1" pin="UART5_TXD" pad="37"/>
<connect gate="G$1" pin="VDD_3V3EXP@1" pad="49"/>
<connect gate="G$1" pin="VDD_3V3EXP@2" pad="50"/>
<connect gate="G$1" pin="VDD_5V@1" pad="51"/>
<connect gate="G$1" pin="VDD_5V@2" pad="52"/>
<connect gate="G$1" pin="VDD_ADC" pad="78"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA03-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="square" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" rot="R90"/>
<text x="-3.81" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MA05-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="square"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-6.35" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
</package>
<package name="MA03-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" shape="square"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<text x="-3.81" y="2.921" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
</package>
<package name="MA02-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="square" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" rot="R90"/>
<text x="-3.81" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
</package>
<package name="MA02-1W">
<wire x1="0" y1="1.524" x2="0" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0" y1="1.524" x2="-2.54" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.524" x2="-2.54" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="4.318" x2="0" y2="4.318" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.524" x2="2.54" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0" y1="1.524" x2="2.54" y2="1.524" width="0.1524" layer="21"/>
<wire x1="0" y1="4.318" x2="2.54" y2="4.318" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="9.779" width="0.6604" layer="21"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="9.779" width="0.6604" layer="21"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.54" y="-2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">1</text>
<text x="1.905" y="2.032" size="1.27" layer="21" ratio="10" rot="R90">2</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="4.318" x2="-0.9398" y2="5.08" layer="21"/>
<rectangle x1="0.9398" y1="4.318" x2="1.6002" y2="5.08" layer="21"/>
<rectangle x1="0.9398" y1="0.8636" x2="1.6002" y2="1.524" layer="21"/>
<rectangle x1="-1.6002" y1="0.8636" x2="-0.9398" y2="1.524" layer="21"/>
<rectangle x1="0.9398" y1="-0.3302" x2="1.6002" y2="0.8636" layer="51"/>
<rectangle x1="-1.6002" y1="-0.3302" x2="-0.9398" y2="0.8636" layer="51"/>
</package>
<package name="MA05-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="square" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" rot="R90"/>
<text x="-6.35" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="MA04-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="square" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" rot="R90"/>
<text x="-5.08" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA03-1">
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<text x="-1.27" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="MA05-2">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="MA03-2">
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="MA02-1">
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<text x="-1.27" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="MA05-1">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="MA04-1">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA03-1" prefix="S" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA03-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA03-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA05-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA05-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA05-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA03-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA03-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA03-2">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA02-1" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA02-1" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="MA02-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="W" package="MA02-1W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA05-1" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA05-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA05-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA04-1" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA04-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-phoenix-508">
<description>&lt;b&gt;Phoenix Connectors&lt;/b&gt;&lt;p&gt;
Grid 5.08 mm&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;pho508a.lbr
&lt;li&gt;pho508b.lbr
&lt;li&gt;pho508c.lbr
&lt;li&gt;pho508d.lbr
&lt;li&gt;pho508e.lbr
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MKDSN1,5/2-5,08">
<description>&lt;b&gt;MKDSN 1,5/ 2-5,08&lt;/b&gt; Printklemme&lt;p&gt;
Nennstrom: 13,5 A&lt;br&gt;
Nennspannung: 250 V&lt;br&gt;
Rastermaß: 5,08 mm&lt;br&gt;
Polzahl: 2&lt;br&gt;
Anschlussart: Schraubanschluss&lt;br&gt;
Montage: Löten&lt;br&gt;
Anschlussrichtung Leiter/Platine: 0 °&lt;br&gt;
Artikelnummer: 1729128&lt;br&gt;
Source: http://eshop.phoenixcontact.com .. 1729128.pdf</description>
<wire x1="-5.1011" y1="-4.05" x2="5.0589" y2="-4.05" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="2.5243" x2="5.0589" y2="2.5243" width="0.2032" layer="21"/>
<wire x1="-2.7211" y1="-0.63" x2="-3.6341" y2="-1.543" width="0.2032" layer="51"/>
<wire x1="-5.1011" y1="-1.9555" x2="-5.1011" y2="-2.4479" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-3.327" x2="-5.2298" y2="-3.327" width="0.2032" layer="21"/>
<wire x1="-5.2298" y1="-2.913" x2="-5.1011" y2="-2.913" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-3.1279" x2="-5.1011" y2="-2.913" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-3.327" x2="-5.1011" y2="-3.1279" width="0.2032" layer="21"/>
<wire x1="-5.6711" y1="-2.763" x2="-5.4081" y2="-2.763" width="0.2032" layer="21"/>
<wire x1="-5.6711" y1="-3.477" x2="-5.4081" y2="-3.477" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-2.913" x2="-5.1011" y2="-2.4479" width="0.2032" layer="21"/>
<wire x1="-5.2298" y1="-3.327" x2="-5.4081" y2="-3.477" width="0.2032" layer="21"/>
<wire x1="-5.6711" y1="-2.763" x2="-5.6711" y2="-3.477" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="2.5243" x2="-5.1011" y2="2.0413" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-1.9555" x2="-5.1011" y2="2.0413" width="0.2032" layer="21"/>
<wire x1="-5.4081" y1="-2.763" x2="-5.2298" y2="-2.913" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-2.4479" x2="-5.1011" y2="-2.4479" width="0.2032" layer="21"/>
<wire x1="-2.5611" y1="-0.47" x2="-2.7211" y2="-0.63" width="0.2032" layer="51"/>
<wire x1="-3.2001" y1="-0.119" x2="-2.5611" y2="0.52" width="0.2032" layer="51"/>
<wire x1="-4.1291" y1="-1.048" x2="-3.2001" y2="-0.119" width="0.2032" layer="51"/>
<wire x1="0.9509" y1="-1.048" x2="1.8799" y2="-0.119" width="0.2032" layer="51"/>
<wire x1="5.0589" y1="-1.9555" x2="5.0589" y2="-2.4479" width="0.2032" layer="21"/>
<wire x1="2.3589" y1="-0.63" x2="1.4459" y2="-1.543" width="0.2032" layer="51"/>
<wire x1="1.8799" y1="-0.119" x2="2.5189" y2="0.52" width="0.2032" layer="51"/>
<wire x1="2.5189" y1="-0.47" x2="2.3589" y2="-0.63" width="0.2032" layer="51"/>
<wire x1="4.0869" y1="1.098" x2="3.1489" y2="0.16" width="0.2032" layer="51"/>
<wire x1="2.6379" y1="0.639" x2="3.5919" y2="1.593" width="0.2032" layer="51"/>
<wire x1="3.1489" y1="0.16" x2="2.5189" y2="-0.47" width="0.2032" layer="51"/>
<wire x1="2.5189" y1="0.52" x2="2.6379" y2="0.639" width="0.2032" layer="51"/>
<wire x1="-1.9311" y1="0.16" x2="-2.5611" y2="-0.47" width="0.2032" layer="51"/>
<wire x1="-2.5611" y1="0.52" x2="-2.4421" y2="0.639" width="0.2032" layer="51"/>
<wire x1="-2.4421" y1="0.639" x2="-1.4881" y2="1.593" width="0.2032" layer="51"/>
<wire x1="-0.9931" y1="1.098" x2="-1.9311" y2="0.16" width="0.2032" layer="51"/>
<wire x1="-5.1011" y1="-3.1279" x2="5.0589" y2="-3.1279" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-3.1279" x2="5.0589" y2="-2.4479" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-1.9555" x2="5.0589" y2="2.0413" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="2.5243" x2="5.0589" y2="2.0413" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="2.5243" x2="5.0589" y2="4.05" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-3.75" x2="-5.1011" y2="-3.327" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-3.75" x2="5.0589" y2="-3.1279" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="4.05" x2="5.0589" y2="4.05" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="2.5243" x2="-5.1011" y2="4.05" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-3.75" x2="-5.1011" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="-5.1011" y1="-3.75" x2="-5.1011" y2="-4.05" width="0.2032" layer="21"/>
<wire x1="5.0589" y1="-3.75" x2="5.0589" y2="-4.05" width="0.2032" layer="21"/>
<wire x1="-3.2001" y1="-0.119" x2="-2.7211" y2="-0.63" width="0.2032" layer="51" curve="65.201851"/>
<wire x1="1.8799" y1="-0.119" x2="2.3589" y2="-0.63" width="0.2032" layer="51" curve="65.201851"/>
<wire x1="3.1489" y1="0.16" x2="2.6379" y2="0.639" width="0.2032" layer="51" curve="65.201851"/>
<wire x1="-1.9311" y1="0.16" x2="-2.4421" y2="0.639" width="0.2032" layer="51" curve="65.201851"/>
<circle x="-2.5611" y="0.025" radius="1.915" width="0.2032" layer="21"/>
<circle x="2.5189" y="0.025" radius="1.915" width="0.2032" layer="21"/>
<pad name="1" x="-2.5611" y="0.025" drill="1.3" diameter="2"/>
<pad name="2" x="2.5189" y="0.025" drill="1.3" diameter="2"/>
<text x="-4.7511" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="6.985" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="KLV">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="3.048" y="-0.889" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.683" size="1.778" layer="96">&gt;VALUE</text>
<pin name="KL" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MKDSN1,5/2-5,08" prefix="X">
<description>&lt;b&gt;MKDSN 1,5/ 2-5,08&lt;/b&gt; Printklemme&lt;p&gt;
Nennstrom: 13,5 A&lt;br&gt;
Nennspannung: 250 V&lt;br&gt;
Rastermaß: 5,08 mm&lt;br&gt;
Polzahl: 2&lt;br&gt;
Anschlussart: Schraubanschluss&lt;br&gt;
Montage: Löten&lt;br&gt;
Anschlussrichtung Leiter/Platine: 0 °&lt;br&gt;
Artikelnummer: 1729128&lt;br&gt;
Source: http://eshop.phoenixcontact.com .. 1729128.pdf</description>
<gates>
<gate name="-1" symbol="KLV" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="KLV" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="MKDSN1,5/2-5,08">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="BBB1" library="BeagleBone-cape" deviceset="BBB-CAPE" device=""/>
<part name="S0" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S1" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S2" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S3" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S4" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S5" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S6" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S7" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S8" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S9" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S10" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S11" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S12" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S13" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S14" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S15" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S16" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S17" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S18" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S19" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S20" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S21" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S22" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S23" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S24" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S25" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S26" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S27" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S28" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S29" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S30" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="S31" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="X1" library="con-phoenix-508" deviceset="MKDSN1,5/2-5,08" device=""/>
<part name="X2" library="con-phoenix-508" deviceset="MKDSN1,5/2-5,08" device=""/>
<part name="UART2" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="UART4" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="ADC" library="con-lstb" deviceset="MA05-2" device=""/>
<part name="SPI" library="con-lstb" deviceset="MA03-2" device=""/>
<part name="PWR" library="con-lstb" deviceset="MA02-1" device=""/>
<part name="I2C1" library="con-lstb" deviceset="MA04-1" device=""/>
<part name="I2C2" library="con-lstb" deviceset="MA04-1" device=""/>
<part name="PWM" library="con-lstb" deviceset="MA03-1" device=""/>
<part name="GPIO" library="con-lstb" deviceset="MA05-1" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="BBB1" gate="G$1" x="35.56" y="68.58"/>
<instance part="S0" gate="G$1" x="106.68" y="132.08"/>
<instance part="S1" gate="G$1" x="132.08" y="132.08"/>
<instance part="S2" gate="G$1" x="157.48" y="132.08"/>
<instance part="S3" gate="G$1" x="182.88" y="132.08"/>
<instance part="S4" gate="G$1" x="106.68" y="111.76"/>
<instance part="S5" gate="G$1" x="132.08" y="111.76"/>
<instance part="S6" gate="G$1" x="157.48" y="111.76"/>
<instance part="S7" gate="G$1" x="182.88" y="111.76"/>
<instance part="S8" gate="G$1" x="106.68" y="91.44"/>
<instance part="S9" gate="G$1" x="132.08" y="91.44"/>
<instance part="S10" gate="G$1" x="157.48" y="91.44"/>
<instance part="S11" gate="G$1" x="182.88" y="91.44"/>
<instance part="S12" gate="G$1" x="106.68" y="71.12"/>
<instance part="S13" gate="G$1" x="132.08" y="71.12"/>
<instance part="S14" gate="G$1" x="157.48" y="71.12"/>
<instance part="S15" gate="G$1" x="182.88" y="71.12"/>
<instance part="S16" gate="G$1" x="106.68" y="50.8"/>
<instance part="S17" gate="G$1" x="132.08" y="50.8"/>
<instance part="S18" gate="G$1" x="157.48" y="50.8"/>
<instance part="S19" gate="G$1" x="182.88" y="50.8"/>
<instance part="S20" gate="G$1" x="106.68" y="30.48"/>
<instance part="S21" gate="G$1" x="132.08" y="30.48"/>
<instance part="S22" gate="G$1" x="157.48" y="30.48"/>
<instance part="S23" gate="G$1" x="182.88" y="30.48"/>
<instance part="S24" gate="G$1" x="106.68" y="10.16"/>
<instance part="S25" gate="G$1" x="132.08" y="10.16"/>
<instance part="S26" gate="G$1" x="157.48" y="10.16"/>
<instance part="S27" gate="G$1" x="182.88" y="10.16"/>
<instance part="S28" gate="G$1" x="106.68" y="-10.16"/>
<instance part="S29" gate="G$1" x="132.08" y="-10.16"/>
<instance part="S30" gate="G$1" x="157.48" y="-10.16"/>
<instance part="S31" gate="G$1" x="182.88" y="-10.16"/>
<instance part="X1" gate="-1" x="15.24" y="-15.24" rot="R180"/>
<instance part="X1" gate="-2" x="15.24" y="-10.16" rot="R180"/>
<instance part="X2" gate="-1" x="55.88" y="-15.24" rot="R180"/>
<instance part="X2" gate="-2" x="55.88" y="-10.16" rot="R180"/>
<instance part="UART2" gate="G$1" x="-86.36" y="119.38"/>
<instance part="UART4" gate="G$1" x="-50.8" y="119.38"/>
<instance part="ADC" gate="G$1" x="-50.8" y="101.6"/>
<instance part="SPI" gate="1" x="-50.8" y="83.82"/>
<instance part="PWR" gate="G$1" x="-53.34" y="71.12"/>
<instance part="I2C1" gate="1" x="-86.36" y="55.88"/>
<instance part="I2C2" gate="1" x="-53.34" y="55.88"/>
<instance part="PWM" gate="G$1" x="-53.34" y="38.1"/>
<instance part="GPIO" gate="G$1" x="-53.34" y="20.32"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="1"/>
<wire x1="190.5" y1="129.54" x2="193.04" y2="129.54" width="0.1524" layer="91"/>
<wire x1="193.04" y1="129.54" x2="193.04" y2="121.92" width="0.1524" layer="91"/>
<pinref part="S23" gate="G$1" pin="1"/>
<wire x1="193.04" y1="121.92" x2="193.04" y2="109.22" width="0.1524" layer="91"/>
<wire x1="193.04" y1="109.22" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
<wire x1="193.04" y1="88.9" x2="193.04" y2="68.58" width="0.1524" layer="91"/>
<wire x1="193.04" y1="68.58" x2="193.04" y2="48.26" width="0.1524" layer="91"/>
<wire x1="193.04" y1="48.26" x2="193.04" y2="27.94" width="0.1524" layer="91"/>
<wire x1="193.04" y1="27.94" x2="190.5" y2="27.94" width="0.1524" layer="91"/>
<pinref part="S31" gate="G$1" pin="1"/>
<wire x1="190.5" y1="-12.7" x2="193.04" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="193.04" y1="-12.7" x2="193.04" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S27" gate="G$1" pin="1"/>
<wire x1="193.04" y1="7.62" x2="193.04" y2="27.94" width="0.1524" layer="91"/>
<wire x1="190.5" y1="7.62" x2="193.04" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S19" gate="G$1" pin="1"/>
<wire x1="190.5" y1="48.26" x2="193.04" y2="48.26" width="0.1524" layer="91"/>
<pinref part="S15" gate="G$1" pin="1"/>
<wire x1="190.5" y1="68.58" x2="193.04" y2="68.58" width="0.1524" layer="91"/>
<pinref part="S11" gate="G$1" pin="1"/>
<wire x1="190.5" y1="88.9" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
<pinref part="S7" gate="G$1" pin="1"/>
<wire x1="190.5" y1="109.22" x2="193.04" y2="109.22" width="0.1524" layer="91"/>
<junction x="193.04" y="109.22"/>
<junction x="193.04" y="88.9"/>
<junction x="193.04" y="68.58"/>
<junction x="193.04" y="48.26"/>
<junction x="193.04" y="27.94"/>
<junction x="193.04" y="7.62"/>
<junction x="193.04" y="129.54"/>
<wire x1="193.04" y1="129.54" x2="198.12" y2="129.54" width="0.1524" layer="91"/>
<label x="198.12" y="129.54" size="1.778" layer="95"/>
<pinref part="S0" gate="G$1" pin="1"/>
<wire x1="114.3" y1="129.54" x2="116.84" y2="129.54" width="0.1524" layer="91"/>
<wire x1="116.84" y1="129.54" x2="116.84" y2="121.92" width="0.1524" layer="91"/>
<pinref part="S20" gate="G$1" pin="1"/>
<wire x1="116.84" y1="121.92" x2="116.84" y2="109.22" width="0.1524" layer="91"/>
<wire x1="116.84" y1="109.22" x2="116.84" y2="88.9" width="0.1524" layer="91"/>
<wire x1="116.84" y1="88.9" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
<wire x1="116.84" y1="68.58" x2="116.84" y2="48.26" width="0.1524" layer="91"/>
<wire x1="116.84" y1="48.26" x2="116.84" y2="27.94" width="0.1524" layer="91"/>
<wire x1="116.84" y1="27.94" x2="114.3" y2="27.94" width="0.1524" layer="91"/>
<pinref part="S28" gate="G$1" pin="1"/>
<wire x1="114.3" y1="-12.7" x2="116.84" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-12.7" x2="116.84" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S24" gate="G$1" pin="1"/>
<wire x1="116.84" y1="7.62" x2="116.84" y2="27.94" width="0.1524" layer="91"/>
<wire x1="114.3" y1="7.62" x2="116.84" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S16" gate="G$1" pin="1"/>
<wire x1="114.3" y1="48.26" x2="116.84" y2="48.26" width="0.1524" layer="91"/>
<pinref part="S12" gate="G$1" pin="1"/>
<wire x1="114.3" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
<pinref part="S8" gate="G$1" pin="1"/>
<wire x1="114.3" y1="88.9" x2="116.84" y2="88.9" width="0.1524" layer="91"/>
<pinref part="S4" gate="G$1" pin="1"/>
<wire x1="114.3" y1="109.22" x2="116.84" y2="109.22" width="0.1524" layer="91"/>
<junction x="116.84" y="109.22"/>
<junction x="116.84" y="88.9"/>
<junction x="116.84" y="68.58"/>
<junction x="116.84" y="48.26"/>
<junction x="116.84" y="27.94"/>
<junction x="116.84" y="7.62"/>
<wire x1="116.84" y1="121.92" x2="142.24" y2="121.92" width="0.1524" layer="91"/>
<junction x="116.84" y="121.92"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="142.24" y1="121.92" x2="167.64" y2="121.92" width="0.1524" layer="91"/>
<wire x1="167.64" y1="121.92" x2="193.04" y2="121.92" width="0.1524" layer="91"/>
<wire x1="139.7" y1="129.54" x2="142.24" y2="129.54" width="0.1524" layer="91"/>
<wire x1="142.24" y1="129.54" x2="142.24" y2="121.92" width="0.1524" layer="91"/>
<pinref part="S21" gate="G$1" pin="1"/>
<wire x1="142.24" y1="121.92" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="142.24" y1="109.22" x2="142.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="142.24" y1="88.9" x2="142.24" y2="68.58" width="0.1524" layer="91"/>
<wire x1="142.24" y1="68.58" x2="142.24" y2="48.26" width="0.1524" layer="91"/>
<wire x1="142.24" y1="48.26" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<wire x1="142.24" y1="27.94" x2="139.7" y2="27.94" width="0.1524" layer="91"/>
<pinref part="S29" gate="G$1" pin="1"/>
<wire x1="139.7" y1="-12.7" x2="142.24" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-12.7" x2="142.24" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S25" gate="G$1" pin="1"/>
<wire x1="142.24" y1="7.62" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<wire x1="139.7" y1="7.62" x2="142.24" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S17" gate="G$1" pin="1"/>
<wire x1="139.7" y1="48.26" x2="142.24" y2="48.26" width="0.1524" layer="91"/>
<pinref part="S13" gate="G$1" pin="1"/>
<wire x1="139.7" y1="68.58" x2="142.24" y2="68.58" width="0.1524" layer="91"/>
<pinref part="S9" gate="G$1" pin="1"/>
<wire x1="139.7" y1="88.9" x2="142.24" y2="88.9" width="0.1524" layer="91"/>
<pinref part="S5" gate="G$1" pin="1"/>
<wire x1="139.7" y1="109.22" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<junction x="142.24" y="109.22"/>
<junction x="142.24" y="88.9"/>
<junction x="142.24" y="68.58"/>
<junction x="142.24" y="48.26"/>
<junction x="142.24" y="27.94"/>
<junction x="142.24" y="7.62"/>
<junction x="142.24" y="121.92"/>
<pinref part="S2" gate="G$1" pin="1"/>
<wire x1="165.1" y1="129.54" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
<wire x1="167.64" y1="129.54" x2="167.64" y2="121.92" width="0.1524" layer="91"/>
<pinref part="S22" gate="G$1" pin="1"/>
<wire x1="167.64" y1="121.92" x2="167.64" y2="109.22" width="0.1524" layer="91"/>
<wire x1="167.64" y1="109.22" x2="167.64" y2="88.9" width="0.1524" layer="91"/>
<wire x1="167.64" y1="88.9" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<wire x1="167.64" y1="68.58" x2="167.64" y2="48.26" width="0.1524" layer="91"/>
<wire x1="167.64" y1="48.26" x2="167.64" y2="27.94" width="0.1524" layer="91"/>
<wire x1="167.64" y1="27.94" x2="165.1" y2="27.94" width="0.1524" layer="91"/>
<pinref part="S30" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-12.7" x2="167.64" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-12.7" x2="167.64" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S26" gate="G$1" pin="1"/>
<wire x1="167.64" y1="7.62" x2="167.64" y2="27.94" width="0.1524" layer="91"/>
<wire x1="165.1" y1="7.62" x2="167.64" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S18" gate="G$1" pin="1"/>
<wire x1="165.1" y1="48.26" x2="167.64" y2="48.26" width="0.1524" layer="91"/>
<pinref part="S14" gate="G$1" pin="1"/>
<wire x1="165.1" y1="68.58" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<pinref part="S10" gate="G$1" pin="1"/>
<wire x1="165.1" y1="88.9" x2="167.64" y2="88.9" width="0.1524" layer="91"/>
<pinref part="S6" gate="G$1" pin="1"/>
<wire x1="165.1" y1="109.22" x2="167.64" y2="109.22" width="0.1524" layer="91"/>
<junction x="167.64" y="109.22"/>
<junction x="167.64" y="88.9"/>
<junction x="167.64" y="68.58"/>
<junction x="167.64" y="48.26"/>
<junction x="167.64" y="27.94"/>
<junction x="167.64" y="7.62"/>
<junction x="167.64" y="121.92"/>
<junction x="193.04" y="121.92"/>
</segment>
<segment>
<pinref part="X2" gate="-2" pin="KL"/>
<wire x1="58.42" y1="-10.16" x2="71.12" y2="-10.16" width="0.1524" layer="91"/>
<label x="66.04" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-2" pin="KL"/>
<wire x1="17.78" y1="-10.16" x2="30.48" y2="-10.16" width="0.1524" layer="91"/>
<label x="25.4" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART2" gate="G$1" pin="1"/>
<wire x1="-78.74" y1="116.84" x2="-71.12" y2="116.84" width="0.1524" layer="91"/>
<label x="-76.2" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART4" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="116.84" x2="-35.56" y2="116.84" width="0.1524" layer="91"/>
<label x="-40.64" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SPI" gate="1" pin="1"/>
<wire x1="-43.18" y1="81.28" x2="-35.56" y2="81.28" width="0.1524" layer="91"/>
<label x="-40.64" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PWR" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="68.58" x2="-38.1" y2="68.58" width="0.1524" layer="91"/>
<label x="-43.18" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C1" gate="1" pin="1"/>
<wire x1="-78.74" y1="50.8" x2="-71.12" y2="50.8" width="0.1524" layer="91"/>
<label x="-76.2" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C2" gate="1" pin="1"/>
<wire x1="-45.72" y1="50.8" x2="-38.1" y2="50.8" width="0.1524" layer="91"/>
<label x="-43.18" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PWM" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="35.56" x2="-38.1" y2="35.56" width="0.1524" layer="91"/>
<label x="-43.18" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GND@7"/>
<wire x1="7.62" y1="129.54" x2="-2.54" y2="129.54" width="0.1524" layer="91"/>
<label x="-2.54" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GND@8"/>
<wire x1="63.5" y1="129.54" x2="73.66" y2="129.54" width="0.1524" layer="91"/>
<label x="71.12" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GND@2"/>
<wire x1="63.5" y1="63.5" x2="73.66" y2="63.5" width="0.1524" layer="91"/>
<label x="71.12" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GND@1"/>
<wire x1="7.62" y1="63.5" x2="-2.54" y2="63.5" width="0.1524" layer="91"/>
<label x="-5.08" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GND@3"/>
<wire x1="7.62" y1="10.16" x2="-2.54" y2="10.16" width="0.1524" layer="91"/>
<label x="-5.08" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GND@4"/>
<wire x1="7.62" y1="7.62" x2="-2.54" y2="7.62" width="0.1524" layer="91"/>
<label x="-5.08" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GND@5"/>
<wire x1="63.5" y1="10.16" x2="73.66" y2="10.16" width="0.1524" layer="91"/>
<label x="71.12" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GND@6"/>
<wire x1="63.5" y1="7.62" x2="73.66" y2="7.62" width="0.1524" layer="91"/>
<label x="71.12" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="15.24" x2="-38.1" y2="15.24" width="0.1524" layer="91"/>
<label x="-43.18" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="V_SERVO" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="2"/>
<wire x1="190.5" y1="132.08" x2="195.58" y2="132.08" width="0.1524" layer="91"/>
<wire x1="195.58" y1="132.08" x2="195.58" y2="124.46" width="0.1524" layer="91"/>
<pinref part="S23" gate="G$1" pin="2"/>
<wire x1="195.58" y1="124.46" x2="195.58" y2="111.76" width="0.1524" layer="91"/>
<wire x1="195.58" y1="111.76" x2="195.58" y2="91.44" width="0.1524" layer="91"/>
<wire x1="195.58" y1="91.44" x2="195.58" y2="71.12" width="0.1524" layer="91"/>
<wire x1="195.58" y1="71.12" x2="195.58" y2="50.8" width="0.1524" layer="91"/>
<wire x1="195.58" y1="50.8" x2="195.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="195.58" y1="30.48" x2="190.5" y2="30.48" width="0.1524" layer="91"/>
<pinref part="S31" gate="G$1" pin="2"/>
<wire x1="190.5" y1="-10.16" x2="195.58" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-10.16" x2="195.58" y2="10.16" width="0.1524" layer="91"/>
<pinref part="S27" gate="G$1" pin="2"/>
<wire x1="195.58" y1="10.16" x2="195.58" y2="30.48" width="0.1524" layer="91"/>
<wire x1="190.5" y1="10.16" x2="195.58" y2="10.16" width="0.1524" layer="91"/>
<pinref part="S19" gate="G$1" pin="2"/>
<wire x1="190.5" y1="50.8" x2="195.58" y2="50.8" width="0.1524" layer="91"/>
<pinref part="S15" gate="G$1" pin="2"/>
<wire x1="190.5" y1="71.12" x2="195.58" y2="71.12" width="0.1524" layer="91"/>
<pinref part="S11" gate="G$1" pin="2"/>
<wire x1="190.5" y1="91.44" x2="195.58" y2="91.44" width="0.1524" layer="91"/>
<pinref part="S7" gate="G$1" pin="2"/>
<wire x1="190.5" y1="111.76" x2="195.58" y2="111.76" width="0.1524" layer="91"/>
<junction x="195.58" y="111.76"/>
<junction x="195.58" y="91.44"/>
<junction x="195.58" y="71.12"/>
<junction x="195.58" y="50.8"/>
<junction x="195.58" y="30.48"/>
<junction x="195.58" y="10.16"/>
<junction x="195.58" y="132.08"/>
<wire x1="195.58" y1="132.08" x2="198.12" y2="132.08" width="0.1524" layer="91"/>
<label x="198.12" y="132.08" size="1.778" layer="95"/>
<pinref part="S0" gate="G$1" pin="2"/>
<wire x1="114.3" y1="132.08" x2="119.38" y2="132.08" width="0.1524" layer="91"/>
<wire x1="119.38" y1="132.08" x2="119.38" y2="124.46" width="0.1524" layer="91"/>
<pinref part="S20" gate="G$1" pin="2"/>
<wire x1="119.38" y1="124.46" x2="119.38" y2="111.76" width="0.1524" layer="91"/>
<wire x1="119.38" y1="111.76" x2="119.38" y2="91.44" width="0.1524" layer="91"/>
<wire x1="119.38" y1="91.44" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
<wire x1="119.38" y1="71.12" x2="119.38" y2="50.8" width="0.1524" layer="91"/>
<wire x1="119.38" y1="50.8" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<wire x1="119.38" y1="30.48" x2="114.3" y2="30.48" width="0.1524" layer="91"/>
<pinref part="S28" gate="G$1" pin="2"/>
<wire x1="114.3" y1="-10.16" x2="119.38" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-10.16" x2="119.38" y2="10.16" width="0.1524" layer="91"/>
<pinref part="S24" gate="G$1" pin="2"/>
<wire x1="119.38" y1="10.16" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<wire x1="114.3" y1="10.16" x2="119.38" y2="10.16" width="0.1524" layer="91"/>
<pinref part="S16" gate="G$1" pin="2"/>
<wire x1="114.3" y1="50.8" x2="119.38" y2="50.8" width="0.1524" layer="91"/>
<pinref part="S12" gate="G$1" pin="2"/>
<wire x1="114.3" y1="71.12" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
<pinref part="S8" gate="G$1" pin="2"/>
<wire x1="114.3" y1="91.44" x2="119.38" y2="91.44" width="0.1524" layer="91"/>
<pinref part="S4" gate="G$1" pin="2"/>
<wire x1="114.3" y1="111.76" x2="119.38" y2="111.76" width="0.1524" layer="91"/>
<junction x="119.38" y="111.76"/>
<junction x="119.38" y="91.44"/>
<junction x="119.38" y="71.12"/>
<junction x="119.38" y="50.8"/>
<junction x="119.38" y="30.48"/>
<junction x="119.38" y="10.16"/>
<wire x1="119.38" y1="124.46" x2="144.78" y2="124.46" width="0.1524" layer="91"/>
<junction x="119.38" y="124.46"/>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="144.78" y1="124.46" x2="170.18" y2="124.46" width="0.1524" layer="91"/>
<wire x1="170.18" y1="124.46" x2="195.58" y2="124.46" width="0.1524" layer="91"/>
<wire x1="139.7" y1="132.08" x2="144.78" y2="132.08" width="0.1524" layer="91"/>
<wire x1="144.78" y1="132.08" x2="144.78" y2="124.46" width="0.1524" layer="91"/>
<pinref part="S21" gate="G$1" pin="2"/>
<wire x1="144.78" y1="124.46" x2="144.78" y2="111.76" width="0.1524" layer="91"/>
<wire x1="144.78" y1="111.76" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<wire x1="144.78" y1="91.44" x2="144.78" y2="71.12" width="0.1524" layer="91"/>
<wire x1="144.78" y1="71.12" x2="144.78" y2="50.8" width="0.1524" layer="91"/>
<wire x1="144.78" y1="50.8" x2="144.78" y2="30.48" width="0.1524" layer="91"/>
<wire x1="144.78" y1="30.48" x2="139.7" y2="30.48" width="0.1524" layer="91"/>
<pinref part="S29" gate="G$1" pin="2"/>
<wire x1="139.7" y1="-10.16" x2="144.78" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-10.16" x2="144.78" y2="10.16" width="0.1524" layer="91"/>
<pinref part="S25" gate="G$1" pin="2"/>
<wire x1="144.78" y1="10.16" x2="144.78" y2="30.48" width="0.1524" layer="91"/>
<wire x1="139.7" y1="10.16" x2="144.78" y2="10.16" width="0.1524" layer="91"/>
<pinref part="S17" gate="G$1" pin="2"/>
<wire x1="139.7" y1="50.8" x2="144.78" y2="50.8" width="0.1524" layer="91"/>
<pinref part="S13" gate="G$1" pin="2"/>
<wire x1="139.7" y1="71.12" x2="144.78" y2="71.12" width="0.1524" layer="91"/>
<pinref part="S9" gate="G$1" pin="2"/>
<wire x1="139.7" y1="91.44" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<pinref part="S5" gate="G$1" pin="2"/>
<wire x1="139.7" y1="111.76" x2="144.78" y2="111.76" width="0.1524" layer="91"/>
<junction x="144.78" y="111.76"/>
<junction x="144.78" y="91.44"/>
<junction x="144.78" y="71.12"/>
<junction x="144.78" y="50.8"/>
<junction x="144.78" y="30.48"/>
<junction x="144.78" y="10.16"/>
<junction x="144.78" y="124.46"/>
<pinref part="S2" gate="G$1" pin="2"/>
<wire x1="165.1" y1="132.08" x2="170.18" y2="132.08" width="0.1524" layer="91"/>
<wire x1="170.18" y1="132.08" x2="170.18" y2="124.46" width="0.1524" layer="91"/>
<pinref part="S22" gate="G$1" pin="2"/>
<wire x1="170.18" y1="124.46" x2="170.18" y2="111.76" width="0.1524" layer="91"/>
<wire x1="170.18" y1="111.76" x2="170.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="170.18" y1="91.44" x2="170.18" y2="71.12" width="0.1524" layer="91"/>
<wire x1="170.18" y1="71.12" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="170.18" y1="50.8" x2="170.18" y2="30.48" width="0.1524" layer="91"/>
<wire x1="170.18" y1="30.48" x2="165.1" y2="30.48" width="0.1524" layer="91"/>
<pinref part="S30" gate="G$1" pin="2"/>
<wire x1="165.1" y1="-10.16" x2="170.18" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-10.16" x2="170.18" y2="10.16" width="0.1524" layer="91"/>
<pinref part="S26" gate="G$1" pin="2"/>
<wire x1="170.18" y1="10.16" x2="170.18" y2="30.48" width="0.1524" layer="91"/>
<wire x1="165.1" y1="10.16" x2="170.18" y2="10.16" width="0.1524" layer="91"/>
<pinref part="S18" gate="G$1" pin="2"/>
<wire x1="165.1" y1="50.8" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
<pinref part="S14" gate="G$1" pin="2"/>
<wire x1="165.1" y1="71.12" x2="170.18" y2="71.12" width="0.1524" layer="91"/>
<pinref part="S10" gate="G$1" pin="2"/>
<wire x1="165.1" y1="91.44" x2="170.18" y2="91.44" width="0.1524" layer="91"/>
<pinref part="S6" gate="G$1" pin="2"/>
<wire x1="165.1" y1="111.76" x2="170.18" y2="111.76" width="0.1524" layer="91"/>
<junction x="170.18" y="111.76"/>
<junction x="170.18" y="91.44"/>
<junction x="170.18" y="71.12"/>
<junction x="170.18" y="50.8"/>
<junction x="170.18" y="30.48"/>
<junction x="170.18" y="10.16"/>
<junction x="170.18" y="124.46"/>
<junction x="195.58" y="124.46"/>
</segment>
<segment>
<pinref part="X1" gate="-1" pin="KL"/>
<wire x1="17.78" y1="-15.24" x2="30.48" y2="-15.24" width="0.1524" layer="91"/>
<label x="20.32" y="-15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="-1" pin="KL"/>
<wire x1="58.42" y1="-15.24" x2="71.12" y2="-15.24" width="0.1524" layer="91"/>
<label x="60.96" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="S0" class="0">
<segment>
<pinref part="S0" gate="G$1" pin="3"/>
<wire x1="114.3" y1="134.62" x2="121.92" y2="134.62" width="0.1524" layer="91"/>
<label x="121.92" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="TIMER7"/>
<wire x1="63.5" y1="121.92" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<label x="71.12" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="S1" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="139.7" y1="134.62" x2="147.32" y2="134.62" width="0.1524" layer="91"/>
<label x="147.32" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="TIMER5"/>
<wire x1="7.62" y1="119.38" x2="-2.54" y2="119.38" width="0.1524" layer="91"/>
<label x="-2.54" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="S2" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="3"/>
<wire x1="165.1" y1="134.62" x2="172.72" y2="134.62" width="0.1524" layer="91"/>
<label x="172.72" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="TIMER6"/>
<wire x1="63.5" y1="119.38" x2="73.66" y2="119.38" width="0.1524" layer="91"/>
<label x="71.12" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="S3" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="3"/>
<wire x1="190.5" y1="134.62" x2="198.12" y2="134.62" width="0.1524" layer="91"/>
<label x="198.12" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO1_13"/>
<wire x1="7.62" y1="116.84" x2="-2.54" y2="116.84" width="0.1524" layer="91"/>
<label x="-2.54" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="S4" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO1_14"/>
<wire x1="63.5" y1="111.76" x2="73.66" y2="111.76" width="0.1524" layer="91"/>
<label x="71.12" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="S4" gate="G$1" pin="3"/>
<wire x1="114.3" y1="114.3" x2="121.92" y2="114.3" width="0.1524" layer="91"/>
<label x="121.92" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="S8" class="0">
<segment>
<pinref part="S8" gate="G$1" pin="3"/>
<wire x1="114.3" y1="93.98" x2="121.92" y2="93.98" width="0.1524" layer="91"/>
<label x="121.92" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_25"/>
<wire x1="63.5" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<label x="71.12" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="S12" class="0">
<segment>
<pinref part="S12" gate="G$1" pin="3"/>
<wire x1="114.3" y1="73.66" x2="121.92" y2="73.66" width="0.1524" layer="91"/>
<label x="121.92" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="UART5_RXD"/>
<wire x1="63.5" y1="83.82" x2="73.66" y2="83.82" width="0.1524" layer="91"/>
<label x="71.12" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="S16" class="0">
<segment>
<pinref part="S16" gate="G$1" pin="3"/>
<wire x1="114.3" y1="53.34" x2="121.92" y2="53.34" width="0.1524" layer="91"/>
<label x="121.92" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO1_12"/>
<wire x1="63.5" y1="116.84" x2="73.66" y2="116.84" width="0.1524" layer="91"/>
<label x="71.12" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="S20" class="0">
<segment>
<pinref part="S20" gate="G$1" pin="3"/>
<wire x1="114.3" y1="33.02" x2="121.92" y2="33.02" width="0.1524" layer="91"/>
<label x="121.92" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO1_29"/>
<wire x1="63.5" y1="99.06" x2="73.66" y2="99.06" width="0.1524" layer="91"/>
<label x="71.12" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="S5" class="0">
<segment>
<pinref part="S5" gate="G$1" pin="3"/>
<wire x1="139.7" y1="114.3" x2="147.32" y2="114.3" width="0.1524" layer="91"/>
<label x="147.32" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO0_27"/>
<wire x1="7.62" y1="109.22" x2="-2.54" y2="109.22" width="0.1524" layer="91"/>
<label x="-2.54" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="S9" class="0">
<segment>
<pinref part="S9" gate="G$1" pin="3"/>
<wire x1="139.7" y1="93.98" x2="147.32" y2="93.98" width="0.1524" layer="91"/>
<label x="147.32" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="UART5_CTSN"/>
<wire x1="7.62" y1="91.44" x2="-2.54" y2="91.44" width="0.1524" layer="91"/>
<label x="-2.54" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="S13" class="0">
<segment>
<pinref part="S13" gate="G$1" pin="3"/>
<wire x1="139.7" y1="73.66" x2="147.32" y2="73.66" width="0.1524" layer="91"/>
<label x="147.32" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_12"/>
<wire x1="7.62" y1="81.28" x2="-2.54" y2="81.28" width="0.1524" layer="91"/>
<label x="-2.54" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="S17" class="0">
<segment>
<pinref part="S17" gate="G$1" pin="3"/>
<wire x1="139.7" y1="53.34" x2="147.32" y2="53.34" width="0.1524" layer="91"/>
<label x="147.32" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="EHRPWM2B"/>
<wire x1="7.62" y1="114.3" x2="-2.54" y2="114.3" width="0.1524" layer="91"/>
<label x="-2.54" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="S21" class="0">
<segment>
<pinref part="S21" gate="G$1" pin="3"/>
<wire x1="139.7" y1="33.02" x2="147.32" y2="33.02" width="0.1524" layer="91"/>
<label x="147.32" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_22"/>
<wire x1="7.62" y1="96.52" x2="-2.54" y2="96.52" width="0.1524" layer="91"/>
<label x="-2.54" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="S22" class="0">
<segment>
<pinref part="S22" gate="G$1" pin="3"/>
<wire x1="165.1" y1="33.02" x2="172.72" y2="33.02" width="0.1524" layer="91"/>
<label x="172.72" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_24"/>
<wire x1="63.5" y1="96.52" x2="73.66" y2="96.52" width="0.1524" layer="91"/>
<label x="71.12" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="S18" class="0">
<segment>
<pinref part="S18" gate="G$1" pin="3"/>
<wire x1="165.1" y1="53.34" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
<label x="172.72" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO0_26"/>
<wire x1="63.5" y1="114.3" x2="73.66" y2="114.3" width="0.1524" layer="91"/>
<label x="71.12" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="S14" class="0">
<segment>
<pinref part="S14" gate="G$1" pin="3"/>
<wire x1="165.1" y1="73.66" x2="172.72" y2="73.66" width="0.1524" layer="91"/>
<label x="172.72" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_13"/>
<wire x1="63.5" y1="81.28" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
<label x="71.12" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="S10" class="0">
<segment>
<pinref part="S10" gate="G$1" pin="3"/>
<wire x1="165.1" y1="93.98" x2="172.72" y2="93.98" width="0.1524" layer="91"/>
<label x="172.72" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="UART5_RTSN"/>
<wire x1="63.5" y1="91.44" x2="73.66" y2="91.44" width="0.1524" layer="91"/>
<label x="71.12" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="S6" class="0">
<segment>
<pinref part="S6" gate="G$1" pin="3"/>
<wire x1="165.1" y1="114.3" x2="172.72" y2="114.3" width="0.1524" layer="91"/>
<label x="172.72" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_1"/>
<wire x1="63.5" y1="109.22" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
<label x="71.12" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="S7" class="0">
<segment>
<pinref part="S7" gate="G$1" pin="3"/>
<wire x1="190.5" y1="114.3" x2="198.12" y2="114.3" width="0.1524" layer="91"/>
<label x="198.12" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="EHRPWM2A"/>
<wire x1="7.62" y1="106.68" x2="-2.54" y2="106.68" width="0.1524" layer="91"/>
<label x="-2.54" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="S11" class="0">
<segment>
<pinref part="S11" gate="G$1" pin="3"/>
<wire x1="190.5" y1="93.98" x2="198.12" y2="93.98" width="0.1524" layer="91"/>
<label x="198.12" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="UART4_RTSN"/>
<wire x1="7.62" y1="88.9" x2="-2.54" y2="88.9" width="0.1524" layer="91"/>
<label x="-2.54" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="S15" class="0">
<segment>
<pinref part="S15" gate="G$1" pin="3"/>
<wire x1="190.5" y1="73.66" x2="198.12" y2="73.66" width="0.1524" layer="91"/>
<label x="198.12" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_10"/>
<wire x1="7.62" y1="78.74" x2="-2.54" y2="78.74" width="0.1524" layer="91"/>
<label x="-2.54" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="S19" class="0">
<segment>
<pinref part="S19" gate="G$1" pin="3"/>
<wire x1="190.5" y1="53.34" x2="198.12" y2="53.34" width="0.1524" layer="91"/>
<label x="198.12" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO1_15"/>
<wire x1="7.62" y1="111.76" x2="-2.54" y2="111.76" width="0.1524" layer="91"/>
<label x="-2.54" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="S23" class="0">
<segment>
<pinref part="S23" gate="G$1" pin="3"/>
<wire x1="190.5" y1="33.02" x2="198.12" y2="33.02" width="0.1524" layer="91"/>
<label x="198.12" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_23"/>
<wire x1="7.62" y1="93.98" x2="-2.54" y2="93.98" width="0.1524" layer="91"/>
<label x="-2.54" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="S24" class="0">
<segment>
<pinref part="S24" gate="G$1" pin="3"/>
<wire x1="114.3" y1="12.7" x2="121.92" y2="12.7" width="0.1524" layer="91"/>
<label x="121.92" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="UART3_RTSN"/>
<wire x1="63.5" y1="88.9" x2="73.66" y2="88.9" width="0.1524" layer="91"/>
<label x="71.12" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="S28" class="0">
<segment>
<pinref part="S28" gate="G$1" pin="3"/>
<wire x1="114.3" y1="-7.62" x2="121.92" y2="-7.62" width="0.1524" layer="91"/>
<label x="121.92" y="-7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_11"/>
<wire x1="63.5" y1="78.74" x2="73.66" y2="78.74" width="0.1524" layer="91"/>
<label x="71.12" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="S25" class="0">
<segment>
<pinref part="S25" gate="G$1" pin="3"/>
<wire x1="139.7" y1="12.7" x2="147.32" y2="12.7" width="0.1524" layer="91"/>
<label x="147.32" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="UART4_CTSN"/>
<wire x1="7.62" y1="86.36" x2="-2.54" y2="86.36" width="0.1524" layer="91"/>
<label x="-2.54" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="S29" class="0">
<segment>
<pinref part="S29" gate="G$1" pin="3"/>
<wire x1="139.7" y1="-7.62" x2="147.32" y2="-7.62" width="0.1524" layer="91"/>
<label x="147.32" y="-7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_8"/>
<wire x1="7.62" y1="76.2" x2="-2.54" y2="76.2" width="0.1524" layer="91"/>
<label x="-2.54" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="S26" class="0">
<segment>
<pinref part="S26" gate="G$1" pin="3"/>
<wire x1="165.1" y1="12.7" x2="172.72" y2="12.7" width="0.1524" layer="91"/>
<label x="172.72" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="UART3_CTSN"/>
<wire x1="63.5" y1="86.36" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<label x="71.12" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="S30" class="0">
<segment>
<pinref part="S30" gate="G$1" pin="3"/>
<wire x1="165.1" y1="-7.62" x2="172.72" y2="-7.62" width="0.1524" layer="91"/>
<label x="172.72" y="-7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_9"/>
<wire x1="63.5" y1="76.2" x2="73.66" y2="76.2" width="0.1524" layer="91"/>
<label x="71.12" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="S27" class="0">
<segment>
<pinref part="S27" gate="G$1" pin="3"/>
<wire x1="190.5" y1="12.7" x2="198.12" y2="12.7" width="0.1524" layer="91"/>
<label x="198.12" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="UART5_TXD"/>
<wire x1="7.62" y1="83.82" x2="-2.54" y2="83.82" width="0.1524" layer="91"/>
<label x="-2.54" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="S31" class="0">
<segment>
<pinref part="S31" gate="G$1" pin="3"/>
<wire x1="190.5" y1="-7.62" x2="198.12" y2="-7.62" width="0.1524" layer="91"/>
<label x="198.12" y="-7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO2_6"/>
<wire x1="7.62" y1="73.66" x2="-2.54" y2="73.66" width="0.1524" layer="91"/>
<label x="-2.54" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_5V" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="VDD_5V@2"/>
<wire x1="63.5" y1="58.42" x2="73.66" y2="58.42" width="0.1524" layer="91"/>
<label x="71.12" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="VDD_5V@1"/>
<wire x1="7.62" y1="58.42" x2="-2.54" y2="58.42" width="0.1524" layer="91"/>
<label x="-7.62" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="SYS_5V" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="SYS_5V@2"/>
<wire x1="63.5" y1="55.88" x2="73.66" y2="55.88" width="0.1524" layer="91"/>
<label x="71.12" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="SYS_5V@1"/>
<wire x1="7.62" y1="55.88" x2="-2.54" y2="55.88" width="0.1524" layer="91"/>
<label x="-7.62" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="SYS_RESETN"/>
<wire x1="63.5" y1="53.34" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
<label x="71.12" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART4_RXD" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="UART4_RXD"/>
<wire x1="7.62" y1="50.8" x2="-2.54" y2="50.8" width="0.1524" layer="91"/>
<label x="-12.7" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART4" gate="G$1" pin="3"/>
<wire x1="-43.18" y1="121.92" x2="-35.56" y2="121.92" width="0.1524" layer="91"/>
<label x="-40.64" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART4_TXD" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="UART4_TXD"/>
<wire x1="7.62" y1="48.26" x2="-2.54" y2="48.26" width="0.1524" layer="91"/>
<label x="-12.7" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART4" gate="G$1" pin="2"/>
<wire x1="-43.18" y1="119.38" x2="-35.56" y2="119.38" width="0.1524" layer="91"/>
<label x="-40.64" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO1_16"/>
<wire x1="7.62" y1="45.72" x2="-2.54" y2="45.72" width="0.1524" layer="91"/>
<label x="-5.08" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SCL" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="I2C1_SCL"/>
<wire x1="7.62" y1="43.18" x2="-2.54" y2="43.18" width="0.1524" layer="91"/>
<label x="-10.16" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C1" gate="1" pin="4"/>
<wire x1="-78.74" y1="58.42" x2="-71.12" y2="58.42" width="0.1524" layer="91"/>
<label x="-76.2" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C2_SCL" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="I2C2_SCL"/>
<wire x1="7.62" y1="40.64" x2="-2.54" y2="40.64" width="0.1524" layer="91"/>
<label x="-10.16" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C2" gate="1" pin="4"/>
<wire x1="-45.72" y1="58.42" x2="-38.1" y2="58.42" width="0.1524" layer="91"/>
<label x="-43.18" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART2_TXD" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="UART2_TXD"/>
<wire x1="7.62" y1="38.1" x2="-2.54" y2="38.1" width="0.1524" layer="91"/>
<label x="-12.7" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART2" gate="G$1" pin="2"/>
<wire x1="-78.74" y1="119.38" x2="-71.12" y2="119.38" width="0.1524" layer="91"/>
<label x="-76.2" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO1_17"/>
<wire x1="7.62" y1="35.56" x2="-2.54" y2="35.56" width="0.1524" layer="91"/>
<label x="-5.08" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO3_21" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO3_21"/>
<wire x1="7.62" y1="33.02" x2="-2.54" y2="33.02" width="0.1524" layer="91"/>
<label x="-10.16" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO" gate="G$1" pin="5"/>
<wire x1="-45.72" y1="25.4" x2="-38.1" y2="25.4" width="0.1524" layer="91"/>
<label x="-43.18" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO3_19" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO3_19"/>
<wire x1="7.62" y1="30.48" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
<label x="-10.16" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO" gate="G$1" pin="4"/>
<wire x1="-45.72" y1="22.86" x2="-38.1" y2="22.86" width="0.1524" layer="91"/>
<label x="-43.18" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_DO" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="SPI1_DO"/>
<wire x1="7.62" y1="27.94" x2="-2.54" y2="27.94" width="0.1524" layer="91"/>
<label x="-10.16" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SPI" gate="1" pin="5"/>
<wire x1="-43.18" y1="86.36" x2="-35.56" y2="86.36" width="0.1524" layer="91"/>
<label x="-40.64" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_SCLK" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="SPI1_SCLK"/>
<wire x1="7.62" y1="25.4" x2="-2.54" y2="25.4" width="0.1524" layer="91"/>
<label x="-12.7" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SPI" gate="1" pin="3"/>
<wire x1="-43.18" y1="83.82" x2="-35.56" y2="83.82" width="0.1524" layer="91"/>
<label x="-40.64" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIN4" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="AIN4"/>
<wire x1="7.62" y1="22.86" x2="-2.54" y2="22.86" width="0.1524" layer="91"/>
<label x="-5.08" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-58.42" y1="106.68" x2="-71.12" y2="106.68" width="0.1524" layer="91"/>
<pinref part="ADC" gate="G$1" pin="10"/>
<label x="-73.66" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIN6" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="AIN6"/>
<wire x1="7.62" y1="20.32" x2="-2.54" y2="20.32" width="0.1524" layer="91"/>
<label x="-5.08" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-43.18" y1="106.68" x2="-35.56" y2="106.68" width="0.1524" layer="91"/>
<pinref part="ADC" gate="G$1" pin="9"/>
<label x="-40.64" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIN2" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="AIN2"/>
<wire x1="7.62" y1="17.78" x2="-2.54" y2="17.78" width="0.1524" layer="91"/>
<label x="-5.08" y="17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ADC" gate="G$1" pin="7"/>
<wire x1="-43.18" y1="104.14" x2="-35.56" y2="104.14" width="0.1524" layer="91"/>
<label x="-40.64" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIN0" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="AIN0"/>
<wire x1="7.62" y1="15.24" x2="-2.54" y2="15.24" width="0.1524" layer="91"/>
<label x="-5.08" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="ADC" gate="G$1" pin="5"/>
<wire x1="-43.18" y1="101.6" x2="-35.56" y2="101.6" width="0.1524" layer="91"/>
<label x="-40.64" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="CLKOUT2" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="CLKOUT2"/>
<wire x1="7.62" y1="12.7" x2="-2.54" y2="12.7" width="0.1524" layer="91"/>
<label x="-10.16" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="17.78" x2="-38.1" y2="17.78" width="0.1524" layer="91"/>
<label x="-43.18" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="GPIO0_7" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO0_7"/>
<wire x1="63.5" y1="12.7" x2="73.66" y2="12.7" width="0.1524" layer="91"/>
<label x="71.12" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GPIO" gate="G$1" pin="3"/>
<wire x1="-45.72" y1="20.32" x2="-38.1" y2="20.32" width="0.1524" layer="91"/>
<label x="-43.18" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIN1" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="AIN1"/>
<wire x1="63.5" y1="15.24" x2="73.66" y2="15.24" width="0.1524" layer="91"/>
<label x="71.12" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-58.42" y1="99.06" x2="-71.12" y2="99.06" width="0.1524" layer="91"/>
<label x="-73.66" y="99.06" size="1.778" layer="95"/>
<pinref part="ADC" gate="G$1" pin="4"/>
</segment>
</net>
<net name="AIN3" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="AIN3"/>
<wire x1="63.5" y1="17.78" x2="73.66" y2="17.78" width="0.1524" layer="91"/>
<label x="71.12" y="17.78" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-58.42" y1="101.6" x2="-71.12" y2="101.6" width="0.1524" layer="91"/>
<label x="-73.66" y="101.6" size="1.778" layer="95"/>
<pinref part="ADC" gate="G$1" pin="6"/>
</segment>
</net>
<net name="AIN5" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="AIN5"/>
<wire x1="63.5" y1="20.32" x2="73.66" y2="20.32" width="0.1524" layer="91"/>
<label x="71.12" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-58.42" y1="104.14" x2="-71.12" y2="104.14" width="0.1524" layer="91"/>
<label x="-73.66" y="104.14" size="1.778" layer="95"/>
<pinref part="ADC" gate="G$1" pin="8"/>
</segment>
</net>
<net name="GND_ADC" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="GNDA_ADC"/>
<wire x1="63.5" y1="22.86" x2="73.66" y2="22.86" width="0.1524" layer="91"/>
<label x="71.12" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-43.18" y1="96.52" x2="-35.56" y2="96.52" width="0.1524" layer="91"/>
<label x="-40.64" y="96.52" size="1.778" layer="95"/>
<pinref part="ADC" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VDD_ADC" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="VDD_ADC"/>
<wire x1="63.5" y1="25.4" x2="73.66" y2="25.4" width="0.1524" layer="91"/>
<label x="71.12" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-43.18" y1="99.06" x2="-35.56" y2="99.06" width="0.1524" layer="91"/>
<label x="-40.64" y="99.06" size="1.778" layer="95"/>
<pinref part="ADC" gate="G$1" pin="3"/>
</segment>
</net>
<net name="SPI1_DI" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="SPI1_DI"/>
<wire x1="63.5" y1="27.94" x2="73.66" y2="27.94" width="0.1524" layer="91"/>
<label x="71.12" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SPI" gate="1" pin="4"/>
<wire x1="-58.42" y1="83.82" x2="-71.12" y2="83.82" width="0.1524" layer="91"/>
<label x="-73.66" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI1_CS0" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="SPI1_CS0"/>
<wire x1="63.5" y1="30.48" x2="73.66" y2="30.48" width="0.1524" layer="91"/>
<label x="71.12" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SPI" gate="1" pin="6"/>
<wire x1="-58.42" y1="86.36" x2="-71.12" y2="86.36" width="0.1524" layer="91"/>
<label x="-73.66" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="UART1_RXD"/>
<wire x1="63.5" y1="33.02" x2="73.66" y2="33.02" width="0.1524" layer="91"/>
<label x="71.12" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="UART1_TXD"/>
<wire x1="63.5" y1="35.56" x2="73.66" y2="35.56" width="0.1524" layer="91"/>
<label x="71.12" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART2_RXD" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="UART2_RXD"/>
<wire x1="63.5" y1="38.1" x2="73.66" y2="38.1" width="0.1524" layer="91"/>
<label x="71.12" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="UART2" gate="G$1" pin="3"/>
<wire x1="-78.74" y1="121.92" x2="-71.12" y2="121.92" width="0.1524" layer="91"/>
<label x="-76.2" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C2_SDA" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="I2C2_SDA"/>
<wire x1="63.5" y1="40.64" x2="73.66" y2="40.64" width="0.1524" layer="91"/>
<label x="71.12" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C2" gate="1" pin="3"/>
<wire x1="-45.72" y1="55.88" x2="-38.1" y2="55.88" width="0.1524" layer="91"/>
<label x="-43.18" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="I2C1_SDA" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="I2C1_SDA"/>
<wire x1="63.5" y1="43.18" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<label x="71.12" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C1" gate="1" pin="3"/>
<wire x1="-78.74" y1="55.88" x2="-71.12" y2="55.88" width="0.1524" layer="91"/>
<label x="-76.2" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="GPIO1_28"/>
<wire x1="63.5" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
<label x="71.12" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD_3V3EXP" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="VDD_3V3EXP@1"/>
<wire x1="7.62" y1="60.96" x2="-2.54" y2="60.96" width="0.1524" layer="91"/>
<label x="-15.24" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="VDD_3V3EXP@2"/>
<wire x1="63.5" y1="60.96" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<label x="71.12" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C1" gate="1" pin="2"/>
<wire x1="-78.74" y1="53.34" x2="-71.12" y2="53.34" width="0.1524" layer="91"/>
<label x="-76.2" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="I2C2" gate="1" pin="2"/>
<wire x1="-45.72" y1="53.34" x2="-38.1" y2="53.34" width="0.1524" layer="91"/>
<label x="-43.18" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SPI" gate="1" pin="2"/>
<wire x1="-58.42" y1="81.28" x2="-71.12" y2="81.28" width="0.1524" layer="91"/>
<label x="-73.66" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-58.42" y1="96.52" x2="-71.12" y2="96.52" width="0.1524" layer="91"/>
<pinref part="ADC" gate="G$1" pin="2"/>
<label x="-73.66" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWR" class="0">
<segment>
<pinref part="PWR" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="71.12" x2="-38.1" y2="71.12" width="0.1524" layer="91"/>
<label x="-43.18" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="BBB1" gate="G$1" pin="PWR_BUT"/>
<wire x1="7.62" y1="53.34" x2="-2.54" y2="53.34" width="0.1524" layer="91"/>
<label x="-5.08" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="EHRPWM1A" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="EHRPWM1A"/>
<wire x1="63.5" y1="48.26" x2="73.66" y2="48.26" width="0.1524" layer="91"/>
<label x="71.12" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PWM" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="38.1" x2="-38.1" y2="38.1" width="0.1524" layer="91"/>
<label x="-43.18" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="EHRPWM1B" class="0">
<segment>
<pinref part="BBB1" gate="G$1" pin="EHRPWM1B"/>
<wire x1="63.5" y1="45.72" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
<label x="71.12" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PWM" gate="G$1" pin="3"/>
<wire x1="-45.72" y1="40.64" x2="-38.1" y2="40.64" width="0.1524" layer="91"/>
<label x="-43.18" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
