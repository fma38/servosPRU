The goal of this project is to implement a servos controller on the BeagleBone.

It is able to drive 32 servos, from 5µs to 5000µs, with a 1µs resolution, @50Hz.

Servos pulses widths can be set directly (the servo moves to its new position as
fast as it can), or within a specified delay. In this last case, it is possible
to synchronise any number of servos, so they start/stop at the same time.

It uses PRU0 to generate pulses widths and sync positions.

See https://www.logre.eu/wiki/Servo_Driver_with_BBB_PRUs (french)
