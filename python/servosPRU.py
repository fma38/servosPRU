# -*- coding: utf-8 -*-

""" ServosPRU.py

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see http://www.gnu.org/licenses/gpl.html

@author: Frédéric Mantegazza
@copyright: (C) 2015-2016 Frédéric Mantegazza
@license: GPL
"""

from pru import PRU0

NB_SERVOS = 32

PULSE_WIDTH_OFFSET = 0
INCREMENT_OFFSET = 32
NB_STEPS_OFFSET = 64


class ServosPRU(object):
    """
    """
    def __init__(self):
        """
        """
        super(ServosPRU, self).__init__()

        self._pru0 = PRU0()
        self._rawPulses = NB_SERVOS * [0]

        self._readRawPulses()

    def _writeRawPulses(self):
        """
        """
        self._pru0.writeDataMemory(PULSE_WIDTH_OFFSET, self._rawPulses)

    def _readRawPulses(self):
        """
        """
        self._rawPulses = list(self._pru0.readDataMemory(PULSE_WIDTH_OFFSET, NB_SERVOS))

    def loadFirmware(self, firmware="./pru0.bin"):
        """
        """
        self._pru0.loadFirmware(firmware)

    def getPulse(self, channel):
        """
        """
        self._readRawPulses()
        return self._rawPulses[channel] >> 16

    def getPulses(self):
        """
        """
        self._readRawPulses()
        return [self._rawPulses[channel] >> 16 for channel in range(NB_SERVOS)]

    def setPulse(self, channel, pulse):
        """
        """
        if not 0 <= channel <= 31:
            raise ValueError("channel must be in range [0-31]")
        if pulse != 0 and not 5 <= pulse <= 5000:
            raise ValueError("pulse must be in range [5-5000], or 0 to disable servo")

        self._rawPulses[channel] = pulse << 16

        self._writeRawPulses()

    def setPulses(self, pulses):
        """

        @param pulses: contains channels num/pulse to move
        @type pulses: dict
        """
        for channel, pulse in pulses.items():
            if not 0 <= channel <= 31:
                raise ValueError("channel must be in range [0-31]")
            if pulse != 0 and not 5 <= pulse <= 5000:
                raise ValueError("pulse must be in range [5-5000], or 0 to disable servo")

            self._rawPulses[channel] = pulse << 16

        self._writeRawPulses()

    def moveTo(self, pulses, delay):
        """ Sync move of channels group.

        @param pulses: contains channels num/pulse to move
        @type pulses: dict
        """
        self._readRawPulses()

        if not 20 <= delay <= 20000:
            raise ValueError("delay must be in range [20-20000] (%d)" % delay)

        nbSteps = delay / 20
        for channel, targetPulse in pulses.items():
            if not 0 <= channel <= 31:
                raise ValueError("channel must be in range [0-31]")
            if not 5 <= targetPulse <= 5000:
                raise ValueError("pulse must be in range [5-5000]")
            currentPulse = self._rawPulses[channel]
            inc = (((targetPulse) << 16) - currentPulse) / nbSteps + 1

            self._pru0.writeDataMemory(INCREMENT_OFFSET+channel, inc)

        # Set nb steps, which also start sequence
        self._pru0.writeDataMemory(NB_STEPS_OFFSET, nbSteps)

        self._pru0.waitEvent()

        # Check final reached positions
        self._readRawPulses()

        #for channel in pulses.keys():
            #print "channel %d reached %d" % (channel, self._rawPulses[channel] >> 16)
