#!/usr/bin/python
# -*- coding: utf-8 -*-

""" pru-loader.py

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see http://www.gnu.org/licenses/gpl.html

@author: Frédéric Mantegazza
@copyright: (C) 2015-2016 Frédéric Mantegazza
@license: GPL
"""

import argparse

from pru import PRU0, PRU1


parser = argparse.ArgumentParser(prog="pru-loader.py",
                                 description="This tool is used to load firmware into PRUs.",
                                 epilog="Under developement...")
parser.add_argument("pru", type=int, default=0,
                    help="PRU num (0/1)")
parser.add_argument("firmware", type=str, default="pru0.bin",
                    help="firmware file path")

# Parse args
args = parser.parse_args()
options = dict(vars(args))

if options['pru'] == 0:
    pru = PRU0()
else:
    pru = PRU1()

pru.loadFirmware(options['firmware'])

