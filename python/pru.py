# -*- coding: utf-8 -*-

""" pru.py

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see http://www.gnu.org/licenses/gpl.html

@author: Frédéric Mantegazza
@copyright: (C) 2015-2016 Frédéric Mantegazza
@license: GPL
"""

import os
import mmap
import struct

import prussdrv

DATA_MEMORY = 0x00000000


class PRU(object):
    """
    """
    def __init__(self):
        """
        """
        super(PRU, self).__init__()

        self._init()

        prussdrv.init()
        prussdrv.open(self._numEvent)
        prussdrv.pruintc_init(prussdrv.getPRUSS_INTC_INITDATA())

        with open("/dev/mem", "r+b") as f:
            self._dataMemory = mmap.mmap(f.fileno(), 8*1024, offset=0x4a300000)

    def __del__(self):
        """
        """
        prussdrv.exit()

    def _init(self):
        """
        """
        raise NotImplementedError

    def loadFirmware(self, firmware="/lib/firmware/pru0.bin"):
        """ Load firmware into PRU
        """
        if not os.path.exists(firmware):
            raise ValueError("file not found (%s)" % firmware)

        prussdrv.exec_program(self._numPRU, firmware)

    def readDataMemory(self, offset, nbWords):
        """ Read words from data memory
        """
        data = struct.unpack("<%di" % nbWords, self._dataMemory[DATA_MEMORY+offset*4:DATA_MEMORY+offset*4+nbWords*4])
        if nbWords == 1:
            return data[0]
        else:
            return data

    def writeDataMemory(self, offset, data):
        """

        @param data: data to write
        @type data: list or tuple
        """
        if not isinstance(data, (list, tuple)):
            data = (data,)
        nbWords = len(data)
        self._dataMemory[DATA_MEMORY+offset*4:DATA_MEMORY+offset*4+nbWords*4] = struct.pack("<%di" % nbWords, *data)

    def waitEvent(self):
        """
        """
        prussdrv.pru_wait_event(self._numEvent)
        prussdrv.pru_clear_event(self._numInterrupt, self._numEvent)


class PRU0(PRU):
    """
    """
    def _init(self):
        self._numPRU = prussdrv.PRU0
        self._numEvent = prussdrv.PRU_EVTOUT_0
        self._numInterrupt = prussdrv.PRU0_ARM_INTERRUPT


class PRU1(PRU):
    """
    """
    def _init(self):
        self._numPRU = prussdrv.PRU1
        self._numEvent = prussdrv.PRU_EVTOUT_1
        self._numInterrupt = prussdrv.PRU1_ARM_INTERRUPT
